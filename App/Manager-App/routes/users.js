const express = require('express');
const router = express.Router();
const auth = require('../utils/auth');
const UserController = require('../controllers/user_controller');

// logout
router.get('/logout', auth.isLogined, UserController.logout);

// get user
router.get('/', auth.isLogined, UserController.user);

//get login page
router.get('/login', auth.isLogouted, UserController.get_login);

// post login
router.post('/login', auth.isLogouted, UserController.post_login);

//add new user request into cache
router.post('/add', auth.isLogined, UserController.add_into_cache);
    
module.exports = router;