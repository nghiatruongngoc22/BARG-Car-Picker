var express = require('express');
var router = express.Router();
const IndexController = require('../controllers/index_controller');
const auth = require('../utils/auth');

/* GET home page. */
router.get('/', auth.isLogined, IndexController.home);

module.exports = router;
