let map, infowindow, infowindowContent, pointMarker, driverMarker, directionsService, directionsDisplay;

function initMap() {
    var center = {
        lat: 10.762670, 
        lng: 106.682558
    };
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: center
    });
    infowindow = new google.maps.InfoWindow();
    infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    // setPoint();
    // showDirectionInMap(data);
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer({
        map: map,
        suppressMarkers : true
    });
}

function cleanMap() {
    if(pointMarker) {
        pointMarker.setMap(null);
        pointMarker = null;
    }
    if(driverMarker) {
        driverMarker.setMap(null);
        driverMarker = null;
    }
    directionsDisplay.setDirections({routes: []});
    // directionsDisplay.setMap(null);
}

function setPoint(data) {
    setUserLocation(data);
    setInfoWindow(data);
}

function setUserLocation(user) {
    let icon = {
        url: '/images/user.png',
        size: new google.maps.Size(20, 35),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
    };
    console.log(user);
    let location = {
        lat: parseFloat(user.lat),
        lng: parseFloat(user.lng)
    };
    pointMarker = new google.maps.Marker({
        position: location,
        map: map,
        icon: icon,
        title: user.customerName,
    });
    pointMarker.addListener('click', function() {
            setInfoWindow(user);
        });
    map.setZoom(16);
    map.setCenter(location);
}

function setInfoWindow(point) {
    if(infowindow) {
        infowindowContent.style.display = 'none';
        infowindow.close();
    }
    setInfoWindowContent(point);
    infowindowContent.style.display = "block";
    infowindow.open(map, pointMarker);
}

function setInfoWindowContent(point) {
    infowindowContent.children['user-name'].textContent = point.customerName;
    infowindowContent.children['user-phone'].textContent = point.phoneNumber;
    infowindowContent.children['user-address'].textContent = point.address;
    infowindowContent.children['point-status'].textContent = 'Status: ' + statusToText(point.status);
    infowindowContent.children['car-type'].textContent = 'Type: ' + typeToText(point.carType);
}

function setDriverLocation(driver) {
    let icon = {
        url: '/images/car.png',
        size: new google.maps.Size(20, 35),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32),
        rotation: 0,
    };
    let location = {
        lat: parseFloat(driver.lat),
        lng: parseFloat(driver.lng)
    };
    driverMarker = new google.maps.Marker({
        position: location,
        map: map,
        icon: icon,
        title: driver.description,
        label: 'A'
    });
    pointMarker.setLabel('B');
}

function showDirectionInMap(data) {
    // directionsDisplay.setMap(map);
    if(infowindow) {
        infowindowContent.style.display = 'none';
        infowindow.close();
    }
    directionsDisplay.setPanel(document.getElementById('direction'));
    let pointLocation = {
        lat: parseFloat(data.customerinfo.lat),
        lng: parseFloat(data.customerinfo.lng)
    };
    let driverLocation = {
        lat: parseFloat(data.car_info.lat),
        lng: parseFloat(data.car_info.lng)
    };
    calculateAndDisplayRoute(driverLocation, pointLocation);
    // setDriverLocation(data.car_info);
}

function calculateAndDisplayRoute(pointA, pointB) {
    directionsService.route({
        origin: pointA,
        destination: pointB,
        avoidTolls: true,
        avoidHighways: false,
        travelMode: google.maps.TravelMode.DRIVING
    }, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            // showStep(response);
        } else {
            console.log('Directions request failed due to ' + status);
        }
    });
}

let markerArray = [];
function showStep(directionResult) {

    for (i = 0; i < markerArray.length; i++) {
        markerArray[i].setMap(null);
    }
    markerArray = [];

    var myRoute = directionResult.routes[0].legs[0];
    attachInstructionText(driverMarker, myRoute.steps[0].instructions);
    for (var i = 1; i < myRoute.steps.length; i++) {
        var marker = new google.maps.Marker({
            position: myRoute.steps[i].start_point, 
            map: map
        });
        attachInstructionText(marker, myRoute.steps[i].instructions);
        markerArray.push(marker);
    }
    attachInstructionText(pointMarker, myRoute.steps[myRoute.steps.length - 1].instructions);
    google.maps.event.trigger(driverMarker, "click");
}

function attachInstructionText(marker, text) {
    marker.addListener('click', function() {
        let stepDisplay = new google.maps.InfoWindow();
        stepDisplay.setContent(text);
        stepDisplay.open(map, marker);
    });
} 