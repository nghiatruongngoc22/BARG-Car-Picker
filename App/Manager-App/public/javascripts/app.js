let selValue;

function getDirection(e) {
    $('#loading-modal').modal('show');
    console.log(selValue);
    if(selValue) {
        $.ajax({
            url: '/users',
            type: 'get',
            data: { id: selValue },
            success: function(result) {
                // showDirectionInList(result);
                if(!result.error && result.car_info) {
                    showDirectionInMap(result);
                } else {
                    let html = `
                    <div class="alert alert-danger">
                        <p>No direction to show</p>
                    </div>
                    `;
                    $('#direction').html(html);
                    console.log('Error:'+result.returnmessage);
                }
            },
            error: function(err) {
                console.log(err);
            },
            complete: function() {
                $('#loading-modal').modal('hide');
            }
        });
    } else {
        alert('Please choose one user request');
        return false;
    }
}

function showDirectionInList(data) {
    let html = '';
    data.forEach(element => {
        html += `<a href="javascript:void(0);" class="list-group-item">
                    <p class="list-group-item-heading"><b>${el}</b></p>
                    <p class="list-group-item-text">...</p>
                </a>`;
    });
    $('#direction .list-group').append(html);
}

function chooseUserRequest(e) {
    console.log('data-status: ' + $(e).attr('data-status'));
    if($(e).attr('data-status') == 'done') {
        //for unselected row
        let $unsel = $(e).parent('tbody').find('tr.selected').removeClass('selected');
        let unselValue = $unsel.find('input[type="radio"]').val();
        console.log('unselected value = ' + unselValue);
            
        //for selected row
        $(e).addClass('selected');

        //get value selected
        let $input = $(e).find('input[type="radio"]');
        selValue = $input.val();
        console.log('selected value = ' + selValue);
        
        //check row seleted
        $input.prop('checked', true);

        $('#loading-modal').modal('show');
        $.ajax({
            url: '/users',
            type: 'get',
            data: { id: selValue },
            success: function(result) {
                console.log(result);
                if(!result.error) {
                    cleanMap();
                    setPoint(result.customerinfo);
                    if(result.returncode == 1) {
                        console.log(result.car_info);
                        setDriverLocation(result.car_info);
                        addDriverUI(result.car_info);
                    } else {
                        let html = `
                        <div class="alert alert-danger">
                            <p>No driver choosen</p>
                        </div>
                        `;
                        $('#driver').html(html);
                    }
                    $('html, body').animate({
                        scrollTop: $('#map').offset().top
                    }, 1000);
                } else {
                    alert('Some thing wrong. Please try again.');
                }
            },
            error: function(err) {
                console.log(err);
            },
            complete: function() {
                $('#loading-modal').modal('hide');
            }
        });
    } else {
        alert('This request has not done yet.');
    }
}

function addUserRequestUI(request) {
    console.log(request);
    let status = statusToText(request.status);
    let type = typeToText(request.carType);
    let html = 
    `<tr data-status="${status}" id="${request.id}" onclick="chooseUserRequest(this);">
        <td class="divider">
            <div class="radio-box">
                <input type="radio" id="radio-box-user-${request.id}" name="user-selected" disabled value="${request.id}">
                <label for="radio-box-user-${request.id}" data-type="user"></label>
            </div>
        </td>
        <td>${request.id}</td>
        <td>${request.customerName}</td>
        <td>${request.phoneNumber}</td>
        <td class="request">${request.address}</td>
        <td class="type">
            <p class="${type.toLowerCase()}">${type}</p>
        </td>
        <td class="regDate">${request.regDate}</td>
        <td class="divider">${request.description}</td>
        <td class="car">${(request.carID == '0') ? '' : request.carID}</td>
        <td class="status">
            <p class="${status}">${status}</p>
        </td>
        <td class="admin">${request.adminActor}</td>
    </tr>`;
    $('.table-user tbody').prepend(html);
}

function addDriverUI(driver) {
    let html = 
    `<h4>${driver.description}</h4>
    <p>
        <i class="glyphicon glyphicon-star"></i>
        <i class="glyphicon glyphicon-star"></i>
        <i class="glyphicon glyphicon-star"></i>
        <i class="glyphicon glyphicon-star"></i>
        <i class="glyphicon glyphicon-star"></i>
    </p>
    <p>
        <i class="glyphicon glyphicon-map-marker"></i> ${driver.pickAddress}
    </p>
    <p><i class="glyphicon glyphicon-phone"></i> Type: ${typeToText(driver.type)}</p>
    <p><i class="glyphicon glyphicon-phone"></i> Distance: ${driver.distance}m</p>`;
    $('#driver > .row').removeClass('hidden');
    $('#driver .driver-info').html(html);
}

function updateUserRequestUI(request, data) {
    let status = statusToText(data.status);
    let elRequest = $('.table-user tr#'+request);
    elRequest.attr('data-status', status);
    elRequest.find('td.status p').text(status).removeClass().addClass(status);

    $('.table-user tr#'+request).find('td.address').text(data.address);

    $('.table-user tr#'+request).find('td.car').text(data.carID);

    $('.table-user tr#'+request).find('td.admin').text(data.adminActor);
}

function addToCache(data) {
    $.ajax({
        url: '/users/add',
        type: 'post',
        data: data,
        success: function(result) {
            console.log("success");
        },
        error: function(err) {
            console.log(err);
        }
    });
}

function removeRequest(e) {
    if(selValue) {
        let ok = confirm('Do you want to remove this request?');
        if(ok) {
            $('.table-user tr#'+selValue).remove();
            selValue = 0;
            cleanMap();
        }
    } else {
        alert('Please choose one user request');
    }
}

function clearAllRequest(e) {
    if($('.table-user tbody > tr').length) {
        let ok = confirm('Do you want to remove all requests?');
        if(ok) {
            $('.table-user tbody > tr').remove();
            cleanMap();
            $('.notify-num').text('0');
        }
    } else {
        alert('No user requests found');
    }
}

function statusToText(statusCode) {
    switch (statusCode) {
        case 0:
            return 'ready';
            break;
        case 1:
            return 'pending';
            break;
        default:
            return 'done';
            break;
    }
}

function typeToText(typeCode) {
    switch(typeCode) {
        case 1:
            return 'Normal';
            break;
        case 2: 
            return 'Premium';
            break;
    }
}

function updateNotifyNum() {
    let num = parseInt($('.notify-num').text()) || 0;
    $('.notify-num').text((++num).toString());
}