// Initialize Firebase
let config = {
    apiKey: "AIzaSyAxU9sfkJp-IQ7VSUjI7F1j4F4zLAla-vc",
    authDomain: "barg-car-picker.firebaseapp.com",
    databaseURL: "https://barg-car-picker.firebaseio.com",
    projectId: "barg-car-picker",
    storageBucket: "barg-car-picker.appspot.com",
    messagingSenderId: "167070661452"
};
firebase.initializeApp(config);

let database = firebase.database();

let userRequest = database.ref('/customerinfo');

userRequest.on('child_added', function(snapshot) {
    if(document.location.href.indexOf('login') == -1) {
        console.log(snapshot.key + ' ' + snapshot.val());
        addUserRequestUI(snapshot.val());
        // addToCache(snapshot.val());
        updateNotifyNum();
    }
});

userRequest.on('child_changed', function(snapshot) {
    if(document.location.href.indexOf('login') == -1) {
        updateUserRequestUI(snapshot.key, snapshot.val());
        // addToCache(snapshot.val());
    }
});