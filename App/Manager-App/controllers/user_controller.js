const axios = require('axios');
const querystring = require('querystring');
const config = require('../config/config');
const cache = require('memory-cache');

const baseUrlAPI = config.baseUrlAPI;

module.exports.user = (req, res, next) => {
    let id = req.query.id;
    let pointInfo = cache.get('point-info-'+id);
    console.log(pointInfo);
    if(pointInfo) {
        return res.json(pointInfo);
    } else {
        let user = cache.get('user-account');
        axios.post(`${baseUrlAPI}/getverifiedinfo`, querystring.stringify({ 
                username: user.username,
                adminsig: user.adminsig,
                id: id
            }))
            .then((response) => {
                let result = response.data;
                console.log(result);
                if(result.returncode == 1) {
                    cache.put('point-info-'+id, result);
                    return res.json(result);
                } else {
                    return res.json({'error': true, 'message': result.returnmessage});
                }
            })
            .catch((err) => console.log(err));
    }
}

module.exports.get_login = (req, res, next) => {
    res.render('login', { title: 'Login Page | Manager' });
}

module.exports.post_login = (req, res, next) => {
    let username = req.body.username;
    let password = req.body.password;
    if(!username || !password) {
        return res.render('login', { title: 'Login Page | Manager', error: 'Username and password are required' });
    } else {
        let user = cache.get('user-account');
        console.log('user-account:' + user);
        if(user && (user.username == username && user.password == password)) {
            return res.redirect('/');
        } else {
            axios.post(`${baseUrlAPI}/login`, querystring.stringify({
                    username: username,
                    password: password,
                    role: 3
                }))
                .then((response) => {
                    let result = response.data;
                    console.log(result);
                    if(result.returncode == 1) {
                        cache.put('user-account', result.admininfo);
                        return res.redirect('/');
                    } else {
                        return res.render('login', { title: 'Login Page | Manager', error: result.returnmessage });
                    }
                })
                .catch((err) => {
                    console.log("Error:" + err);
                    return res.render('login', { title: 'Login Page | Manager', error: `Error: ${err}` });
                });
        }
    }
}

module.exports.logout = (req, res, next) => {
    let user = cache.get('user-account');
    console.log(user);
    axios.post(`${baseUrlAPI}/logout`, querystring.stringify({
        username: user.username,
        adminsig: user.adminsig
    }))
    .then((response) => {
        let result = response.data;
        console.log(result);
        if(result.returncode == 1) {
            cache.del('user-account');
            return res.redirect('/users/login');
        } else {
            console.log(result.returnmessage);
            return res.redirect('/');
        }
    })
    .catch((err) => {
        console.log("Error:" + err);
        return res.render('login', { title: 'Login Page | Manager', error: `Error: ${err}` });
    });
}

module.exports.add_into_cache = (req, res, next) => {
    let id = req.body.id;
    cache.put('point-info-'+id, req.body);
    return res.json({'error': false});
}