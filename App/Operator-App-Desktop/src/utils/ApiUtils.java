/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

public class ApiUtils {

    private static final Logger logger = Logger.getLogger("ApiUtils");
    
    public static final int ROLE = 1;
    
    public static final String LOGIN_URL = "http://localhost:8999/barg/login";
    public static final String LOGOUT_URL = "http://localhost:8999/barg/logout";
    public static final String ADD_INFO = "http://localhost:8999/barg/addcustomerinfo";
    public static final String GET_INFO = "http://localhost:8999/barg/getallcustomerinfobyphone";

    public static String login(String username, String password) throws Exception {
        String url = "";

        try {
            url = LOGIN_URL;
            Map<String, String> params = new HashMap<>();

            params.put("username", username);
            params.put("password", password);
            params.put("role", String.valueOf(ROLE));
            
            
            String resultMsg = "";
            resultMsg = HttpUtils.postParams(url, params, 20000);

            logger.info("login response: " + resultMsg);

            return resultMsg;

        } catch (Exception ex) {
            logger.error("geoCodingGoogle parse json : " + ex);
            return "";
        }
    }
    
    
    public static String logout(String username, String sig) throws Exception {
        String url = "";

        try {
            url = LOGOUT_URL;
            Map<String, String> params = new HashMap<>();

            params.put("username", username);
            params.put("adminsig", sig);
            params.put("role", String.valueOf(ROLE));
            
            
            String resultMsg = "";
            resultMsg = HttpUtils.postParams(url, params, 20000);

            logger.info("logout response: " + resultMsg);

            return resultMsg;

        } catch (Exception ex) {
            logger.error("logout parse json : " + ex);
            return "";
        }
    }
    
    
    public static String addCustomerInfo(String customername, String phonenumber,
            String adminsig, String username, String address, String descr, int carType) throws Exception {
        String url = "";

        try {
            url = ADD_INFO;
            Map<String, String> params = new HashMap<>();

            params.put("customername", customername);
            params.put("phonenumber", phonenumber);
            params.put("address", address);
            params.put("adminsig", adminsig);
            params.put("username", username);
            params.put("descr", descr);
            params.put("cartype", String.valueOf(carType));
            
            String resultMsg = "";
            resultMsg = HttpUtils.postParams(url, params, 20000);

            logger.info("addCustomerInfo response: " + resultMsg);

            return resultMsg;

        } catch (Exception ex) {
            logger.error("addCustomerInfo parse json : " + ex);
            return "";
        }
    }
    
    
    public static String getAllCustomerInfoByPhone(String phonenumber,
            String adminsig, String username) throws Exception {
        String url = "";

        try {
            url = GET_INFO;
            Map<String, String> params = new HashMap<>();

            params.put("phonenumber", phonenumber);
            params.put("adminsig", adminsig);
            params.put("username", username);
            
            String resultMsg = "";
            resultMsg = HttpUtils.postParams(url, params, 20000);

            logger.info("getAllCustomerInfoByPhone response: " + resultMsg);

            return resultMsg;

        } catch (Exception ex) {
            logger.error("addCustomerInfo parse json : " + ex);
            return "";
        }
    }
    
}
