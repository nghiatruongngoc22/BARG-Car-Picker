import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'
import { HTTP } from '../http.js';
import querystring from 'querystring';

Vue.use(Vuex)

export const store = new Vuex.Store({
	state: {
		user: null,
		map: null,
		point: null,
		driver: null,
		loading: false
	},
	getters: {
		adminsig(state) {
			return state.user.adminsig;
		},
		user(state) {
			return state.user;
		},
		map(state) {
			return state.map;
		},
		point(state) {
			return state.point;
		},
		driver(state) {
			return state.driver;
		},
		loading(state) {
			return state.loading;
		}
	},
	mutations: {
		setUser(state, payload) {
			state.user = payload;
		},
		setMap(state, payload) {
			state.map = payload;
		},
		setPoint(state, payload) {
			state.point = payload;
		},
		updatePoint(state, payload) {
			if(payload.status != undefined) {
				state.point.status = payload.status;
			}
			if(payload.formatted_address) {
				state.point.address = payload.address;
			}
			if(payload.lat) {
				state.point.lat = payload.lat;
			}
			if(payload.lng) {
				state.point.lng = payload.lng;
			}
		},
		setDriver(state, payload) {
			state.driver = payload;
		},
		setLoading(state, payload) {
			state.loading = payload;
		}
	},
	actions: {
		geocode({commit, getters}, payload) {
			HTTP.post('geocoding', querystring.stringify({
				username: getters.user.username,
                adminsig: getters.adminsig,
                id: getters.point.id,
		        address: payload.address
			})).then((response) => {
				if(response.data.returncode == 1) {
					commit('updatePoint', response.data);
				}
				console.log(response.data);
			}).catch((error) => {
					console.log(error);
				});
		},
		reverseGeocode({commit, getters}, payload) {
			HTTP.post('reversegeocoding', querystring.stringify({
				username: getters.user.username,
                adminsig: getters.adminsig,
                id: getters.point.id,
		        lat: payload.lat,
        		lng: payload.lng
			})).then((response) => {
				if(response.data.returncode == 1) {
					commit('updatePoint', response.data);
				}
				console.log(response.data);
			}).catch((error) => {
					console.log(error);
				});
		},
		loadDriver({commit, getters}) {
			console.log(getters.point.id);
			HTTP.post('getverifiedinfo', querystring.stringify({
				username: getters.user.username,
                adminsig: getters.adminsig,
                id: getters.point.id
			})).then((response) => {
				let result = response.data;
				console.log(response.data);
                if(result.returncode == 1) {
                	commit('setDriver', result.car_info);
                } else {
                	commit('setDriver', null);
                }
			}).catch((error) => {
					console.log(error);
				});
		},
	},
})