let markers = [];
let map, infowindow, infowindowContent, userMarker;
let drivers = [];

function initMap() {
    var center = {
        lat: 10.762670, 
        lng: 106.682558
    };
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: center
    });

    var searchBox = document.getElementById('search-box');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchBox);
    // setDriverLocations(drivers);

    infowindowContent = document.getElementById('infowindow-content');
    infowindow = new google.maps.InfoWindow();
    
    map.addListener('click', function(event) {
        let $input = $('.table-user tr input[type="radio"]:checked');
        if($input.length) {
            if(markers.length) {
                setMapMarkers(null);
                markers.length = 0;
            }
            reverseGeocode(event.latLng.lat(), event.latLng.lng(), $input.val());
        }
    });
}

function setDriverLocations(drs) {
    if(markers.length) {
        setMapMarkers(null);
        markers.length = 0;
    }
    infowindow.setContent(infowindowContent);
    drivers = drs;
    drivers.forEach(function(driver) {
        let location = { 
            lat: driver.lat, 
            lng: driver.lng 
        };
        let marker = new google.maps.Marker({
            position: location,
            map: map,
            title: driver.description,
        });
        marker.addListener('click', function() {
            showInfoWindow(this, driver);
        });
        rotateCarMarker(marker);
        markers.push(marker);
    });
    return markers;
}

function setMapMarkers(map) {
    markers.forEach(function(marker) {
        marker.setMap(map);
    });
}

function showDriver(position) {
    drivers.forEach(function(driver, index) {
        if(driver.id == position) {
            showInfoWindow(markers[index], driver);
        }
    });
}

function showInfoWindow(marker, object) {
    if(infowindow) {
        infowindowContent.style.display = 'none';
        infowindow.close();
    }
    // map.setZoom(19);
    setInfoWindowContent(object);
    map.setCenter(marker.getPosition());
    infowindow.open(map, marker);
}

function setInfoWindowContent(object) {
    console.log(object);
    infowindow.setContent(infowindowContent);
    infowindowContent.children['driver-name'].textContent = object.name || object.description;
    infowindowContent.children['driver-phone'].textContent = object.phone;
    infowindowContent.children['driver-type'].textContent = (object.type) ? 'Type: ' + typeToText(object.type) : '';
    infowindowContent.children['driver-distance'].textContent = (object.distance) ? ('Distance:' + object.distance + 'm') : '';
    infowindowContent.children['place-address'].textContent = object.address;
    infowindowContent.style.display = "block";
}

function setUserLocation(data) {
    if(userMarker) {
        userMarker.setMap(null);
        userMarker = null;
    }
    let location = { 
        lat: parseFloat(data.lat),
        lng: parseFloat(data.lng)
    };
    map.setCenter(location);
    let icon = {
        url: '/images/user.png',
        size: new google.maps.Size(20, 35),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
    };
    userMarker = new google.maps.Marker({
        position: location,
        map: map,
        icon: icon
    });
    let user = data.user.customerinfo;
    if(user != null) {
        let info = {
            name: user.customerName,
            phone: user.phoneNumber,
            address: data.formatted_address,
            type: user.carType
        }
        showInfoWindow(userMarker, info);
        userMarker.addListener('click', function() {
            if(infowindow) {
                infowindowContent.style.display = 'none';
                infowindow.close();
            }
            showInfoWindow(this, info);
        });
    }
}

function clearAllItemAfterConfirm() {
    if(userMarker) {
        userMarker.setMap(null);
        userMarker = '';
    }
    if(markers.length) {
        setMapMarkers(null);
        markers.length = 0;
    }
    if(infowindow) {
        infowindowContent.style.display = 'none';
        infowindow.close();
    }
    $('input#search-input').val('');
    initUITabDrivers(true);
}

function geocode(address) {
    if(markers.length) {
        setMapMarkers(null);
        markers.length = 0;
    }
    $('#loading-modal').modal('show');
    $.ajax({
        url: '/map/geocode',
        method: 'get',
        data: {
            address: address,
            id: selValue
        },
        success: function(result) {
            $('#loading-modal').modal('hide');
            if(!result.error) {
                setUserLocation(result);
                $('input#search-input').val(result.formatted_address);
            } else {
                console.log('Error:'+result.message);
                alert('No address found');
            }
            initUITabDrivers((result.error) ? true : false);
            updateCache('clear', 'cars');
        },
        error: function(err) {
            console.log(err);
        }
    });
}

function reverseGeocode(lat, lng, id) {
    $('#loading-modal').modal('show');
    $.ajax({
        url: '/map/reverse-geocode',
        method: 'get',
        data: {
            lat: lat,
            lng: lng,
            id: id
        },
        success: function(result) {
            $('#loading-modal').modal('hide');
            if(!result.error) {
                setUserLocation(result);
                $('input#search-input').val(result.formatted_address);
            } else {
                console.log('Error:'+result.message);
                alert('No address found');
            }
            initUITabDrivers((result.error) ? true : false);
            updateCache('clear', 'cars');
        },
        error: function(err) {
            console.log(err);
        }
    });
}

$(document).ready(function() {
    $('#btn-search').on('click', function(e) {
        if($('.table-user tr input[type="radio"]:checked').length) {
            let address = $(this).siblings('#search-input')[0].value;
            if(address) {
                geocode(address);
            }        
        } else {
            alert('Please choose one user request');
        }
    });
    $('#search-input').on('keydown', function(e) {
        if (e.which == 13 || e.keyCode == 13) {
            if($('.table-user tr input[type="radio"]:checked').length) {
                let address = $(this)[0].value;
                if(address) {
                    geocode(address);
                }
            } else {
                alert('Please choose one user request');
            }
        }
    });
});

function rotateCarMarker(marker) {
    let deg = parseInt(Math.random() * 360);
    let src = RotateIcon.makeIcon("/images/car-icon.png")
            .setRotation({deg: deg})
            .getUrl();
    marker.setOptions({
        icon: src
    });
}

var RotateIcon = function(options){
    this.options = options || {};
    this.rImg = new Image();
    this.rImg.src = this.options.url || '/images/car-icon.png';
    this.options.width = this.options.height || this.rImg.height || 50;
    this.options.height = this.options.height || this.rImg.height || 50;
    canvas = document.createElement("canvas");
    canvas.width = this.options.width;
    canvas.height = this.options.height;
    this.context = canvas.getContext("2d");
    this.canvas = canvas;
};

RotateIcon.makeIcon = function(url) {
    return new RotateIcon({url: url});
};

RotateIcon.prototype.setRotation = function(options){
    var canvas = this.context,
        angle = options.deg ? options.deg * Math.PI / 180 : 0,
        centerX = this.options.width/2,
        centerY = this.options.height/2;

    canvas.clearRect(0, 0, this.options.width, this.options.height);
    canvas.save();
    canvas.translate(centerX, centerY);
    canvas.rotate(angle);
    canvas.translate(-centerX, -centerY);
    canvas.drawImage(this.rImg, 0, 0);
    canvas.restore();
    return this;
};

RotateIcon.prototype.getUrl = function(){
    return this.canvas.toDataURL('image/png');
};