import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueFire from 'vuefire'

import { store } from './store'
import auth from './auth'

import Home from './components/home.vue'
import Login from './components/login.vue'

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueFire);

const routes = [
	{ path: '/', component: Home, beforeEnter: auth },
	{ path: '/login', component: Login },
];

const router = new VueRouter({
	routes: routes,
	mode: 'history'
});

new Vue({
  el: '#app',
  render: h => h(App),
  router: router,
  store: store
})