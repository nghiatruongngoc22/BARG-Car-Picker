import { store } from './store'

export default (from, to, next) => {
	if(store.getters.car) {
		next();
	} else {
		next('/login');		
	}
}