import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'
import { HTTP } from '../http.js';
import querystring from 'querystring';

Vue.use(Vuex)

export const store = new Vuex.Store({
	state: {
		car: null,
		user: null,
		showModal: false,
		map: null,
		point: null
	},
	getters: {
		car(state) {
			return state.car;
		},
		sig(state) {
			return state.car.sig;
		},
		user(state) {
			return state.user;
		},
		showModal(state) {
			return state.showModal;
		},
		map(state) {
			return state.map;
		},
		point(state) {
			return state.point;
		}
	},
	mutations: {
		setCar(state, payload) {
			state.car = payload;
		},
		updateCar(state, payload) {
			if(payload.lat) {
				state.car.lat = payload.lat;
			}
			if(payload.lng) {
				state.car.lng = payload.lng;
			}
			if(payload.pickAddress) {
				state.car.pickAddress = payload.pickAddress;
			}
			if(payload.isBusy != undefined) {
				state.car.isBusy = payload.isBusy;
			}
		},
		setUser(state, payload) {
			state.user = payload;
		},
		setShowModal(state, payload) {
			state.showModal = payload;
		},
		setMap(state, payload) {
			state.map = payload;
		},
		setPoint(state, payload) {
			state.point = payload;
		}
	},
	actions: {
		updateCar({commit, getters}, payload) {
			let data = {
				carID: getters.car.id,
				carsig: getters.sig,
				driverName: getters.car.driverName
			};
			if(payload.lat && payload.lng) {
				data.lat = payload.lat;
				data.lng = payload.lng;
			}
			if(payload.isBusy) {
				data.isBusy = payload.isBusy;
			}
			HTTP.post('updatecarlocation', querystring.stringify(data)).then((response) => {
				commit('updateCar', payload);
				console.log(response.data);
			}).catch((error) => {
					console.log(error);
				});
		},
		beginTravelling({commit, getters}) {
			HTTP.post('begintraveling', querystring.stringify({
				carid: getters.car.id,
				carsig: getters.sig,
				drivername: getters.car.driverName,
				id: getters.user.id
			})).then((response) => {
					commit('updateCar', {
						isBusy: 1
					});
					console.log(response.data);
				})
				.catch((error) => {
					console.log(error);
				});
		},
		endTravelling({commit, getters}) {
			HTTP.post('endtraveling', querystring.stringify({
				carid: getters.car.id,
				carsig: getters.sig,
				drivername: getters.car.driverName,
				id: getters.user.id
			})).then((response) => {
					commit('updateCar', {
						isBusy: 0
					});
					commit('setUser', null);
					console.log(response.data);
				})
				.catch((error) => {
					console.log(error);
				});
		},
		loadUserFromFB({commit}, payload) {
			let userId = payload.id;
			firebase.database().ref('customerinfo/'+userId).once('value')
					.then((data) => {
						commit('setUser', data.val());
					})
					.catch((error) => {
						console.log(error);
					});
		},
	},
})