const express = require('express');
const router = express.Router();
const auth = require('../utils/auth');
const DriverController = require('../controllers/driver_controller');

// get list driver
router.get('/', auth.isLogined, DriverController.drivers);

// confirm driver
router.get('/confirm', auth.isLogined, DriverController.confirm);

//change status
router.post('/status', auth.isLogined, DriverController.change_status);

// confirm driver
router.get('/clear', auth.isLogined, DriverController.clear_cache);

module.exports = router;