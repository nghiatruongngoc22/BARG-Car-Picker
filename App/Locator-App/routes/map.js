const express = require('express');
const router = express.Router();
const auth = require('../utils/auth');
const MapController = require('../controllers/map_controller');

// geocode
router.get('/geocode', auth.isLogined, MapController.geocode);

// reverse geocode
router.get('/reverse-geocode', auth.isLogined, MapController.reverse_geocode);
    
module.exports = router;