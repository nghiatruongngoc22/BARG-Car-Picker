module.exports.isLogined = (req, res, next) => {
    if(res.locals.isLogined) {
        next();
    } else {
        // next();
        return res.redirect('/users/login');
    }
}

module.exports.isLogouted = (req, res, next) => {
    if(!res.locals.isLogined) {
        next();
    } else {
        return res.redirect('/');
    }
}