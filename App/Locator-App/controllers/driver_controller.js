const axios = require('axios');
const querystring = require('querystring');
const cache = require('memory-cache');
const config = require('../config/config');

const baseUrlAPI = config.baseUrlAPI;
    
module.exports.drivers = (req, res, next) => {
    let id = req.query.id;
    let result = cache.get('list-drivers-' + id);
    if(result) {
        return res.json(result);
    } else {    
        let user = cache.get('user-account');
        console.log(user);
        axios.post(`${baseUrlAPI}/availablecarlist`, querystring.stringify({
                username: user.username,
                adminsig: user.adminsig,
                id: id  
            }))
            .then((response) => {
                let result = response.data;
                console.log(result);
                if(result.returncode == 1) {
                    cache.put('list-drivers-'+id, result.carlist);
                    return res.json(result.carlist);
                } else {
                    return res.json({'error': true, 'message': result.returnmessage});
                }
            })
            .catch((err) => console.log(err));
    }
}

module.exports.confirm = (req, res, next) => {
    let id = req.query.id;
    let user = cache.get('user-account');
    let carList = cache.get('list-drivers-'+id);
    let data = {'carlist': carList};
    axios.post(`${baseUrlAPI}/verifycustomerinfo`, querystring.stringify({
            username: user.username,
            adminsig: user.adminsig,
            id: id,
            carlist: JSON.stringify(data)
        }))
        .then((response) => {
            let result = response.data;
            result['car'] = carList[0].id;
            console.log(result);
            return res.json(result);
        })
        .catch((err) => console.log(err));
}

module.exports.change_status = (req, res, next) => {
    axios.post(`${baseUrlAPI}/drivers/status`, req.body)
        .then((result) => {
            console.log(result);
            res.json(result);
        })
        .catch((err) => console.log(err));
}

module.exports.clear_cache = (req, res, next) => {
    let id = req.query.id;
    cache.del('list-drivers-'+id);
    return res.json({'error': false});
}