const axios = require('axios');
const querystring = require('querystring');
const config = require('../config/config');
const cache = require('memory-cache');

const baseUrlAPI = config.baseUrlAPI;

module.exports.geocode = (req, res, next) => {
    let user = cache.get('user-account');
    console.log(req.query);
    console.log(user);
    axios.post(`${baseUrlAPI}/geocoding`, querystring.stringify({
        username: user.username,
        adminsig: user.adminsig,
        address: req.query.address,
        id: req.query.id
    }))
    .then((response) => {
        let result = response.data;
        console.log(result);
        if(result.returncode == 1) {
            result['user'] = cache.get('point-info-'+req.query.id);
            return res.json(result);
        } else {
            return res.json({'error': true, 'message': result.returnmessage});
        }
    })
    .catch((err) => {
        console.log("Error:" + err);
        return res.json({'error': true});
    });
}

module.exports.reverse_geocode = (req, res, next) => {
    let user = cache.get('user-account');
    axios.post(`${baseUrlAPI}/reversegeocoding`, querystring.stringify({
        username: user.username,
        adminsig: user.adminsig,
        id: req.query.id,
        lat: req.query.lat,
        lng: req.query.lng
    }))
    .then((response) => {
        let result = response.data;
        console.log(result);
        if(result.returncode == 1) {
            result['user'] = cache.get('point-info-'+req.query.id);
            return res.json(result);
        } else {
            console.log(result.returnmessage);
            return res.json({'error': true, 'message': result.returnmessage});
        }
    })
    .catch((err) => {
        console.log("Error:" + err);
        return res.json({'error': true});
    });
}