const axios = require('axios');
const querystring = require('querystring');
const config = require('../config/config');
const cache = require('memory-cache');

const baseUrlAPI = config.baseUrlAPI;

module.exports.user = (req, res, next) => {
    let data = req.query.data;
    let status = parseInt(req.query.status);
    let pointInfo = cache.get('point-info-'+data);
    console.log(pointInfo);
    if(status == 0 && pointInfo) {
        pointInfo['returncode'] = 1;
        return res.json(pointInfo);
    } else {
        let user = cache.get('user-account');
        axios.post(`${baseUrlAPI}/getcustomerinfo`, querystring.stringify({ 
                username: user.username,
                adminsig: user.adminsig,
                id: data
            }))
            .then((response) => {
                let result = response.data;
                console.log(result);
                if(result.returncode == 1) {
                    cache.put('point-info-'+data, result);
                } else if(result.returncode == -4 && pointInfo.adminActor) {
                    result['adminActor'] = pointInfo.adminActor;
                }
                return res.json(result);
            })
            .catch((err) => console.log(err));
    }
}

module.exports.get_login = (req, res, next) => {
    res.render('login', { title: 'Login Page | Locator' });
}

module.exports.post_login = (req, res, next) => {
    let username = req.body.username;
    let password = req.body.password;
    if(!username || !password) {
        return res.render('login', { title: 'Login Page | Locator', error: 'Username and password are required' });
    } else {
        let user = cache.get('user-account');
        console.log('user-account:' + user);
        if(user && (user.username == username && user.password == password)) {
            return res.redirect('/');
        } else {
            axios.post(`${baseUrlAPI}/login`, querystring.stringify({
                    username: username,
                    password: password,
                    role: 2
                }))
                .then((response) => {
                    let result = response.data;
                    console.log(result);
                    if(result.returncode == 1) {
                        cache.put('user-account', result.admininfo);
                        return res.redirect('/');
                    } else {
                        return res.render('login', { title: 'Login Page | Locator', error: result.returnmessage });
                    }
                })
                .catch((err) => {
                    console.log("Error:" + err);
                    return res.render('login', { title: 'Login Page | Locator', error: `Error: ${err}` });
                });
        }
    }
}

module.exports.logout = (req, res, next) => {
    let user = cache.get('user-account');
    console.log(user);
    axios.post(`${baseUrlAPI}/logout`, querystring.stringify({
        username: user.username,
        adminsig: user.adminsig
    }))
    .then((response) => {
        let result = response.data;
        console.log(result);
        if(result.returncode == 1) {
            cache.del('user-account');
        } else {
            console.log(result.returnmessage);
        }
        return res.json(result);
    })
    .catch((err) => {
        console.log("Error:" + err);
        return res.json({error: true, returnmessage: `Error: ${err}`});
    });
}

module.exports.change_status = (req, res, next) => {
    let user = cache.get('user-account');
    console.log(req.body);
    axios.post(`${baseUrlAPI}/markcustomerinfo`, querystring.stringify({
        username: user.username,
        adminsig: user.adminsig,
        id: req.body.request,
        status: req.body.status
    }))
    .then((response) => {
        let result = response.data;
        console.log(result);
        if(result.returncode == 1) {
            res.json(result);
        } else {
            res.json({'error': true, 'message': result.returnmessage});
        }
    })
    .catch((err) => console.log(err));
}

module.exports.add_into_cache = (req, res, next) => {
    let id = req.body.id;
    cache.put('point-info-'+id, req.body);
    return res.json({'error': false});
}