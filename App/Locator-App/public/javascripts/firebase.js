// Initialize Firebase
let config = {
    apiKey: "AIzaSyAxU9sfkJp-IQ7VSUjI7F1j4F4zLAla-vc",
    authDomain: "barg-car-picker.firebaseapp.com",
    databaseURL: "https://barg-car-picker.firebaseio.com",
    projectId: "barg-car-picker",
    storageBucket: "barg-car-picker.appspot.com",
    messagingSenderId: "167070661452"
};
firebase.initializeApp(config);

let database = firebase.database();

let userRequests = database.ref('/customerinfo');
userRequests.on('child_added', function(snapshot) {
    console.log(snapshot.key + ' ' + snapshot.val());
    if(document.location.href.indexOf('login') == -1) {
        addUserRequestUI(snapshot.val());
        updateCache('add', snapshot.val());
        if(snapshot.val().status == 0) {
            ++num;
            updateNotifyNum();
        }
    }
});
userRequests.on('child_changed', function(snapshot) {
    if(document.location.href.indexOf('login') == -1) {
        updateUserRequestUI(snapshot.key, snapshot.val());
        updateCache('update', snapshot.val());
    }
});

let carStatuses = database.ref('/cars');
carStatuses.on('child_changed', function(snapshot) {
    console.log(snapshot.key + ' ' + snapshot.val());
    updateCarStatus(snapshot.key, snapshot.val().status);
    // updateCache('clear', 'cars');
});