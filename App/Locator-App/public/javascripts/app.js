let selValue, num = 0;

function removeRequest(e) {
    if(selValue) {
        let ok = confirm('Do you want to remove this request?');
        if(ok) {
            $('.table-user tr#'+selValue).remove();
        }
    } else {
        alert('Please choose a user request');
    }
}

function clearAllRequest(e) {
    if($('.table-user tbody > tr').length) {
        let ok = confirm('Do you want to remove all requests?');
        if(ok) {
            $('.table-user tbody > tr').remove();
            if(selValue) {
                updateDBUserRequestStatus(selValue, 0);
                selValue = 0;
                clearAllItemAfterConfirm();
                num = 0;
                $('.notify-num').text('0');
            }
        }
    } else {
        alert('No user requests found');
    }
}

function findCar(e) {
    if(selValue) {
        $('#loading-modal').modal('show');
        $.ajax({
            url: '/drivers',
            method: 'get',
            data: { id: selValue },
            success: function(result) {
                $('#loading-modal').modal('hide');
                if(!result.error && result.length > 0) {
                    showListDriverUI(result);
                    setDriverLocations(result);
                } else {
                    console.log('Error:'+result.message);
                    $('#btn-confirm').addClass('hidden');
                    $('#list-cars').removeClass('hidden');
                    alert('No driver found');
                    $('.table-driver tbody').html('<div class="alert alert-danger">No driver found</div>');
                    $(e).prop('disabled', true);
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    } else {
        alert('Please choose one user request');
    }
}

function verifyRequest() {
    let ok = confirm('Do you want to confirm this request?');
    if(ok) {
        $.ajax({
            url: '/drivers/confirm',
            method: 'get',
            data: { id: selValue },
            success: function(result) {
                if(result.returncode == 1) {
                    // alert('Car #' + result.car + ' is choosen');
                    selValue = 0;
                    $('html, body').animate({
                        scrollTop: $('.panel').first().offset().top
                    }, 1000);
                    clearAllItemAfterConfirm();
                } else {
                    alert('No car found with your request!');
                }
            },
            error: function(err) {
                console.log(err);
                alert("Something wrong happened, please try again!");
            }
        });
    }   
}

function showListDriverUI(drivers) {
    let html = '';
    drivers.forEach(driver => {
        let rating = Math.floor(Math.random() * 5) + 1,
            ratingHtml = '', i;
        for(i = 1; i <= rating; i++) {
            ratingHtml += `<a href="javascript:void(0);" class="star star-checked pull-left">
                                <i class="glyphicon glyphicon-star"></i>
                            </a>`;
        }
        for(let j = i; j <= 5; j++) {
            ratingHtml += `<a href="javascript:void(0);" class="star pull-left">
                                <i class="glyphicon glyphicon-star"></i>
                            </a>`;
        }
        let type = typeToText(driver.type);
        html += `
        <tr data-status="ready" id="${driver.id}" onclick="chooseDriver(this);">
            <td>
                <div class="radio-box">
                    <input type="radio" id="radio-box-${driver.id}" name="driver-selected" value="${driver.id}">
                    <label for="radio-box-${driver.id}" data-type="driver"></label>
                </div>
            </td>
            <td>
                <div class="media">
                    <a href="javascript:void(0);" class="pull-left">
                        <img src="/images/driver.png" alt="Driver Image" class="media-photo">
                    </a>
                    <div class="media-body">
                        <h4 class="driver-name status">
                            ${driver.description}
                            <span class="ready">ready</span>
                        </h4>
                        <div class="summary">
                            <div class="rating">${ratingHtml}</div>
                            <div class="pull-left">
                                <p class="type ${type.toLowerCase()}">Type: ${type}</p>
                                <p>Distance: ${driver.distance}m</p>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        `;
    });
    $('#list-cars').removeClass('hidden');
    $('#btn-confirm').removeClass('hidden');
    $('#list-cars .table-driver > tbody').html(html);
}

function initUITabDrivers(isSelect) {
    $('#btn-find-car button').prop('disabled', false);
    if(isSelect) {
        $('#btn-find-car button').prop('disabled', true);
    }
    $('#list-cars').addClass('hidden');
    $('#btn-confirm').addClass('hidden');
}

function chooseDriver(e) {
    console.log('data-status: ' + $(e).attr('data-status'));
    if($(e).attr('data-status') == 'ready') {
        //for unselected row
        let $unsel = $(e).parent('tbody').find('tr[data-status*="selected"]').attr('data-status', 'ready').removeClass('selected');
        $unsel.find('.status').children().text('ready').removeClass().addClass('ready');
        
        //for selected row
        $(e).attr('data-status', 'selected').addClass('selected');
        $(e).find('.status').children().text('selected').removeClass().addClass('selected');
        let $input = $(e).find('input[type="radio"]');
        let value = $input.val();
        $input.prop('checked', true);
        
        // updateDBCarStatus(value, 'pending');
        showDriver(value);
    }
}

function addUserRequestUI(request) {
    let status = statusToText(request.status);
    let type = typeToText(request.carType);
    let html = 
    `<tr data-status="${status}" id="${request.id}" onclick="chooseUserRequest(this);">
        <td class="divider">
            <div class="radio-box">
                <input type="radio" id="radio-box-user-${request.id}" name="user-selected" disabled value="${request.id}">
                <label for="radio-box-user-${request.id}" data-type="user"></label>
            </div>
        </td>
        <td>${request.id}</td>
        <td>${request.customerName}</td>
        <td>${request.phoneNumber}</td>
        <td class="request">${request.address}</td>
        <td class="type">
            <p class="${type.toLowerCase()}">${type}</p>
        </td>
        <td class="regDate">${request.regDate}</td>
        <td class="divider">${request.description}</td>
        <td class="car">${(request.carID == 0) ? '' : request.carID}</td>
        <td class="status">
            <p class="${status}">${status}</p>
        </td>
        <td class="admin">${request.adminActor}</td>
    </tr>`;
    $('.table-user tbody').prepend(html);
}

function chooseUserRequest(e) {
    console.log('data-status: ' + $(e).attr('data-status'));
    let status = $(e).attr('data-status');
    if(status == 'ready' || status == 'pending') {
        //change status
            //for unselected row
            let $unsel = $(e).parent('tbody').find('tr[data-status*="selected"]').attr('data-status', 'ready').removeClass('selected');
            $unsel.find('.status').children().text('ready').removeClass().addClass('ready');
            let unselValue = $unsel.find('input[type="radio"]').val();
            console.log('unselected value = ' + unselValue);
            
            //for selected row
            $(e).attr('data-status', 'pending').addClass('selected');
            $(e).find('.status').children().text('pending').removeClass().addClass('pending');

        //get value selected
        let $input = $(e).find('input[type="radio"]');
        let value = $input.val();
        console.log('selected value = ' + value);
        selValue = value;
        
        //determine type choosen: user-request or driver
        let type = $(e).find('label').attr('data-type');
        console.log('type ' + type);
        switch (type) {
            case 'user':
                let data = { 'data': value };
                if(status == 'ready') {
                    data['status'] = 0;
                    --num;
                } else if(status == 'pending') {
                    data['status'] = 1;
                }
                $.ajax({
                    url: '/users',
                    data: data,
                    type: 'get',
                    success: function(data) {
                        console.log(data);
                        if(data.returncode == 1) {
                            //check row seleted
                            $input.prop('checked', true);
                            updateDBUserRequestStatus(value, 1);
                            $('input#search-input').val(data.address || data.customerinfo.address);
                            if(unselValue != undefined) {
                                updateDBUserRequestStatus(unselValue, 0);
                            }
                            // scroll to map
                            $('html, body').animate({
                                scrollTop: $('#map').offset().top
                            }, 1000);
                        } else if(data.returncode == -4) {
                            console.log('Error:'+data.returnmessage);
                            alert(`It\'s proccessd by locator ${data.adminActor.toUpperCase()}. Please choose another user request`);
                        } else {
                            ++num;
                        }
                    },
                    error: function(err) {
                        console.log(err);
                    },
                    complete: function() {
                        updateNotifyNum();
                    }
                });
                break;
            case 'driver':
                // updateDBCarStatus(value, 'pending');
                showDriver(value);
                break;
        }
    } else {
        alert('It\'s done! Please choose another user request');
    }
}

function updateUserRequestUI(request, data) {
    let status = statusToText(data.status);
    let elRequest = $('.table-user tr#'+request);
    let oldStatus = elRequest.attr('data-status');
    if(status != oldStatus) {
        if(oldStatus == 'ready') {
            --num;
        } else if(status == 'ready') {
            ++num;
        }
        updateNotifyNum();
    }
    elRequest.attr('data-status', status);
    elRequest.find('td.status p').text(status).removeClass().addClass(status);

    $('.table-user tr#'+request).find('td.request').text(data.address);

    $('.table-user tr#'+request).find('td.car').text((data.carID == 0) ? '' : data.carID);

    $('.table-user tr#'+request).find('td.admin').text(data.adminActor);
}

function updateCarStatus(car, newStatus) {
    let status = statusToText(newStatus);
    let car = $('.table-driver tr#'+request);
    car.attr('data-status', status);
    car.find('.driver-name span').text(status).removeClass().addClass(status);
}

function updateDBUserRequestStatus(request, newStatus) {
    let data = { 
        'request': request,
        'status': newStatus 
    };
    ajaxCaller('/users/status', data, 'post');
}

function updateDBCarStatus(car, newStatus) {
    let data = { 
        'car': car,
        'status': newStatus 
    };
    ajaxCaller('/drivers/status', data, 'post');
}

function updateCache(type, ...args) {
    switch (type) {
        case 'add':
        case 'update':
            ajaxCaller('/users/add', args[0], 'post');
            break;
        case 'clear':
            if(args[0] == 'cars') {
                ajaxCaller('/drivers/clear', { id: selValue }, 'get');
            } else {
                ajaxCaller('/users/remove', { key: args[1] }, 'get');
            }
            break;
    }
}

function ajaxCaller(...args) {
    console.log(args);
    $.ajax({
        url: args[0],
        data: args[1],
        type: args[2],
        success: function(data) {
            if(typeof args[3] == "function") {
                args[3](data);
            } else {
                console.log(data);
            }
        },
        error: function(err) {
            console.log(err);
        }
    });
}

function statusToText(statusCode) {
    switch (statusCode) {
        case 4: 
            return 'finished';
            break;
        case 3: 
            return 'begin';
            break;
        case 2:
            return 'verified';
            break;
        case 1:
            return 'processing';
            break;
        default:
            return 'ready';
            break;  
    }
}

function typeToText(typeCode) {
    switch(typeCode) {
        case 1:
            return 'Normal';
            break;
        case 2: 
            return 'Premium';
            break;
    }
}

$(document).ready(function() {
    window.onbeforeunload = function(e) {
        if(document.location.href.indexOf('login') == -1) {
            if(selValue) {
                updateDBUserRequestStatus(selValue, 0);
            }
        }
        return undefined;
    }    
});

function logout() {
    if(selValue) {
        updateDBUserRequestStatus(selValue, 0);
    }
    ajaxCaller('/users/logout', null, 'get', function(result) {
        if(result.returncode == 1) {
            window.location.href = "/users/login";
        } else {
            alert('Something wrong. Please try again');
            window.location.href = '/';
        }
    });
}

function updateNotifyNum() {
    // num = parseInt($('.notify-num').text()) || 0;
    $('.notify-num').text((num).toString());
}