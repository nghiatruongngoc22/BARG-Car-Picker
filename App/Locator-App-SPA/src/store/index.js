import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'
import { HTTP } from '../http.js';
import querystring from 'querystring';

Vue.use(Vuex)

export const store = new Vuex.Store({
	state: {
		user: null,
		showModal: false,
		map: null,
		point: null,
		drivers: [],
		driverSelected: null,
		loading: false
	},
	getters: {
		adminsig(state) {
			return state.user.adminsig;
		},
		user(state) {
			return state.user;
		},
		showModal(state) {
			return state.showModal;
		},
		map(state) {
			return state.map;
		},
		point(state) {
			return state.point;
		},
		drivers(state) {
			return state.drivers;
		},
		driverSelected(state) {
			return state.driverSelected;
		},
		loading(state) {
			return state.loading;
		}
	},
	mutations: {
		setUser(state, payload) {
			state.user = payload;
		},
		setShowModal(state, payload) {
			state.showModal = payload;
		},
		setMap(state, payload) {
			state.map = payload;
		},
		setPoint(state, payload) {
			state.point = payload;
		},
		updatePoint(state, payload) {
			if(payload.status != undefined) {
				state.point.status = payload.status;
			}
			if(payload.formatted_address) {
				state.point.address = payload.address;
			}
			if(payload.lat) {
				state.point.lat = payload.lat;
			}
			if(payload.lng) {
				state.point.lng = payload.lng;
			}
		},
		setDrivers(state, payload) {
			state.drivers = payload;
		},
		setDriverSelected(state, payload) {
			state.driverSelected = payload;
		},
		setLoading(state, payload) {
			state.loading = payload;
		}
	},
	actions: {
		updatePoint({commit, getters}, payload) {
			HTTP.post('markcustomerinfo', querystring.stringify({
				username: getters.user.username,
                adminsig: getters.adminsig,
                id: getters.point.id,
                status: payload.status
			})).then((response) => {
				commit('updatePoint', payload);
				console.log(response.data);
			}).catch((error) => {
					console.log(error);
				});
		},
		geocode({commit, getters}, payload) {
			HTTP.post('geocoding', querystring.stringify({
				username: getters.user.username,
                adminsig: getters.adminsig,
                id: getters.point.id,
		        address: payload.address
			})).then((response) => {
				if(response.data.returncode == 1) {
					commit('updatePoint', response.data);
				}
				console.log(response.data);
			}).catch((error) => {
					console.log(error);
				});
		},
		reverseGeocode({commit, getters}, payload) {
			HTTP.post('reversegeocoding', querystring.stringify({
				username: getters.user.username,
                adminsig: getters.adminsig,
                id: getters.point.id,
		        lat: payload.lat,
        		lng: payload.lng
			})).then((response) => {
				if(response.data.returncode == 1) {
					commit('updatePoint', response.data);
				}
				console.log(response.data);
			}).catch((error) => {
					console.log(error);
				});
		},
		findDrivers({commit, getters}) {
			commit('setLoading', true);
			HTTP.post('availablecarlist', querystring.stringify({
				username: getters.user.username,
                adminsig: getters.adminsig,
                id: getters.point.id
			})).then((response) => {
				let result = response.data;
				console.log(response.data);
                if(result.returncode == 1) {
                    commit('setDrivers', result.carlist);
                } else {
                    commit('setDrivers', null);
                }
				commit('setLoading', false);
			}).catch((error) => {
					console.log(error);
				});
		},
		verifiedRequest({commit, getters}) {
	        HTTP.post('verifycustomerinfo', querystring.stringify({
				username: getters.user.username,
                adminsig: getters.adminsig,
                id: getters.point.id,
                carlist: JSON.stringify(getters.drivers)
			})).then((response) => {
				let result = response.data;
                commit('setDrivers', null);
                if(result.returncode == 1) {
                	alert('Request ' + getters.point.id + ' has been verified');
                } else {
                	alert('No car found with request ' + getters.point.id);
                }
                commit('setPoint', null);
				console.log(response.data);
			}).catch((error) => {
					console.log(error);
				});
		},
		// loadUserFromFB({commit}, payload) {
		// 	let userId = payload.id;
		// 	firebase.database().ref('customerinfo/'+userId).once('value')
		// 			.then((data) => {
		// 				commit('setUser', data.val());
		// 			})
		// 			.catch((error) => {
		// 				console.log(error);
		// 			});
		// },
	},
})