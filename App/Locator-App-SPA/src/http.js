var axios = require('axios');

export const HTTP = axios.create({
  baseURL: `http://localhost:8999/barg/`,
  headers: {
    "Accept": "application/json",
    "X-Requested-With": "XMLHttpRequest",
    "Authorization": ''
  },
});