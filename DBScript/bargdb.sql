-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2017 at 11:57 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bargdb`
--
CREATE DATABASE IF NOT EXISTS `bargdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bargdb`;

-- --------------------------------------------------------

--
-- Table structure for table `adminrole`
--

CREATE TABLE `adminrole` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `adminSig` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminrole`
--

INSERT INTO `adminrole` (`id`, `username`, `password`, `description`, `role`, `adminSig`) VALUES
(1, 'nghiatn', '123456', 'Admin Operator', 1, '6eea0df7c6cc27b09abd84d32e152a6cde087d19a367ae281af15eba8ede41c2'),
(2, 'namnnp', '456789', 'Admin Locator', 2, '933c13bc3dddc271f9376049942db843ff8c135d2cdfdf42a3aafba38e8f5684'),
(3, 'nghiatruong', '111111', 'Admin Observer', 3, '8f9a3722dedcb3052c4cf9305509f10338dc7fa9f7b9fee7977b0e1ded5b3994'),
(4, 'admin', 'admin', NULL, 2, '12dd4aa832960fac6f50cd6a837e68968bb6a2e33a41a43774d6a2af2fd994fc');

-- --------------------------------------------------------

--
-- Table structure for table `car`
--

CREATE TABLE `car` (
  `id` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `pickAddress` varchar(500) DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `isBusy` int(11) DEFAULT '0',
  `lng` double DEFAULT NULL,
  `lat` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car`
--

INSERT INTO `car` (`id`, `description`, `pickAddress`, `type`, `isBusy`, `lng`, `lat`) VALUES
(1, 'Car number 1', '', 2, 0, 106.67964167406078, 10.763952908726337),
(2, 'Car number 2', '', 1, 0, 106.68467415392365, 10.760042328452737),
(3, 'Car number 3', '', 2, 0, 106.68469702707246, 10.765168335097187),
(4, 'Car number 4', '', 2, 0, 106.68161112407077, 10.761131542005055),
(5, 'Car number 5', '80 Trần Hưng Đạo, Phạm Ngũ Lão, Quận 1, Hồ Chí Minh, Vietnam', 1, 1, 106.68842192867187, 10.763730738070809),
(6, 'Car number 6', '', 1, 0, 106.68217513245415, 10.762851473028466),
(7, 'Car number 7', '', 2, 0, 106.684248833304, 10.761555195769416),
(8, 'Car number 8', '', 1, 0, 106.68220170710265, 10.760094076263975),
(9, 'Car number 9', '', 1, 0, 106.684764743727, 10.763516549321096),
(10, 'Car number 10', '80 Trần Hưng Đạo, Phạm Ngũ Lão, Quận 1, Hồ Chí Minh, Vietnam', 1, 0, 106.68778291554672, 10.760536669878517),
(11, 'Car number 11', '', 2, 0, 106.67477029019906, 10.7620387336608),
(12, 'Car number 12', '', 1, 0, 106.6776945938161, 10.759783259636917),
(13, 'Car number 13', '', 1, 0, 106.67963967315187, 10.757480656080368),
(14, 'Car number 14', '', 1, 0, 106.68060029122432, 10.758444641858391),
(15, 'Car number 15', '', 2, 0, 106.68149468278432, 10.76388091121924),
(16, 'Car number 16', '', 2, 0, 106.6787276608502, 10.76100774190499),
(17, 'Car number 17', '', 1, 0, 106.68421167954071, 10.766699539923081),
(18, 'Car number 18', '', 1, 0, 106.6791961173806, 10.766511827471385),
(19, 'Car number 19', '', 1, 0, 106.68453133701624, 10.76160187566849),
(20, 'Car number 20', '', 1, 0, 106.67656202335877, 10.759917495547267),
(21, 'Car number 21', '', 2, 0, 106.68190372514476, 10.761851921958693),
(22, 'Car number 22', '', 2, 0, 106.68751695976893, 10.760294270548263),
(23, 'Car number 23', '', 1, 0, 106.6823479742775, 10.761806281759075),
(24, 'Car number 24', '', 1, 0, 106.67999958443552, 10.762739658102143),
(25, 'Car number 25', '', 2, 0, 106.68458529787054, 10.761827832737549),
(26, 'Car number 26', '', 2, 0, 106.68337146174439, 10.764802101210753),
(27, 'Car number 27', '', 2, 0, 106.68392758941701, 10.760593540891808),
(28, 'Car number 28', '', 1, 0, 106.68140938442164, 10.763534757597219),
(29, 'Car number 29', '', 1, 0, 106.68347204016872, 10.762700094533),
(30, 'Car number 30', '', 1, 0, 106.68042231547417, 10.765684848439923);

-- --------------------------------------------------------

--
-- Table structure for table `customerinfor`
--

CREATE TABLE `customerinfor` (
  `id` int(11) NOT NULL,
  `customerName` varchar(200) NOT NULL,
  `lng` double NOT NULL DEFAULT '0',
  `lat` double NOT NULL DEFAULT '0',
  `phoneNumber` varchar(45) CHARACTER SET latin1 NOT NULL,
  `carID` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `address` varchar(200) DEFAULT NULL,
  `adminActor` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `regDate` bigint(20) DEFAULT '0',
  `carType` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customerinfor`
--

INSERT INTO `customerinfor` (`id`, `customerName`, `lng`, `lat`, `phoneNumber`, `carID`, `status`, `address`, `adminActor`, `description`, `regDate`, `carType`) VALUES
(20, 'Nam Nguyen a', 0, 0, '0987654321', 0, 0, '135 THĐạo, Q5', '', 'cần gấp', 1511520415154, 1),
(21, 'Nam Nguyen', 0, 0, '0987654321', 0, 0, '135 THĐạo, Q5', '', 'cần gấp', 1511520859416, 1);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `value`, `description`) VALUES
(1, 1, 'Operator Admin'),
(2, 2, 'Locator Admin'),
(3, 3, 'Observer Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminrole`
--
ALTER TABLE `adminrole`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- Indexes for table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customerinfor`
--
ALTER TABLE `customerinfor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminrole`
--
ALTER TABLE `adminrole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `car`
--
ALTER TABLE `car`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `customerinfor`
--
ALTER TABLE `customerinfor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
