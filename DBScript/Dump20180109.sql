CREATE DATABASE  IF NOT EXISTS `bargdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bargdb`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bargdb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adminrole`
--

DROP TABLE IF EXISTS `adminrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adminrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `adminSig` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adminrole`
--

LOCK TABLES `adminrole` WRITE;
/*!40000 ALTER TABLE `adminrole` DISABLE KEYS */;
INSERT INTO `adminrole` VALUES (1,'nghiatn','123456','Admin Operator',1,'6168c0dccd687578b1a031ecc30c319fdc6e2209798fbd98431291cdeeaeeb32'),(2,'namnnp','456789','Admin Locator',2,'b450c766c899e23761b7de72629adac1e8703ae653eba84633b3579d7d892f01'),(3,'nghiatruong','111111','Admin Observer',3,NULL);
/*!40000 ALTER TABLE `adminrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car`
--

DROP TABLE IF EXISTS `car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `pickAddress` varchar(500) DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `isBusy` int(11) DEFAULT '0',
  `lng` double DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `driverAccountName` varchar(200) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `sig` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car`
--

LOCK TABLES `car` WRITE;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` VALUES (1,'Car number 1','',2,0,106.68016812343042,10.768837804042713,'Driver_1','123456',NULL),(2,'Car number 2','',2,0,106.68687721973407,10.757192608620693,'Driver_2','123456',NULL),(3,'Car number 3','',2,0,106.67985136053889,10.761072439382824,'Driver_3','123456',NULL),(4,'Car number 4','',1,0,106.68040093945774,10.765600164633936,'Driver_4','123456',NULL),(5,'Car number 5','',1,0,106.68285324719554,10.763042296214874,'Driver_5','123456',NULL),(6,'Car number 6','',1,0,106.67829348393869,10.763892076130505,'Driver_6','123456',NULL),(7,'Car number 7','',2,0,106.68360022691677,10.758998024220139,'Driver_7','123456',NULL),(8,'Car number 8','',2,0,106.68403653237708,10.763278289888294,'Driver_8','123456',NULL),(9,'Car number 9','',2,0,106.68125144404958,10.765486424608431,'Driver_9','123456',NULL),(10,'Car number 10','',2,0,106.68400165989496,10.761677011462325,'Driver_10','123456',NULL),(11,'Car number 11','',2,0,106.68115440895917,10.764042088617016,'Driver_11','123456',NULL),(12,'Car number 12','',1,0,106.68304906097858,10.760594956295062,'Driver_12','123456',NULL),(13,'Car number 13','',2,0,106.68406321718042,10.759847267018154,'Driver_13','123456',NULL),(14,'Car number 14','',1,0,106.68271798657348,10.76290320628214,'Driver_14','123456',NULL),(15,'Car number 15','',2,0,106.68844992615327,10.76262822439319,'Driver_15','123456',NULL),(16,'Car number 16','',2,0,106.68270936029721,10.761777545560163,'Driver_16','123456',NULL),(17,'Car number 17','',2,0,106.68250342114804,10.764470312145777,'Driver_17','123456',NULL),(18,'Car number 18','',2,0,106.68772808497866,10.762542713969568,'Driver_18','123456',NULL),(19,'Car number 19','',2,0,106.68162680971345,10.770414274570918,'Driver_19','123456',NULL),(20,'Car number 20','',2,0,106.68352924426378,10.762045825139058,'Driver_20','123456',NULL),(21,'Car number 21','',2,0,106.68261058601935,10.762504998440173,'Driver_21','123456',NULL),(22,'Car number 22','',1,0,106.68185739321079,10.763440490124289,'Driver_22','123456',NULL),(23,'Car number 23','',1,0,106.6804591433824,10.762767423234463,'Driver_23','123456',NULL),(24,'Car number 24','',1,0,106.68086001366859,10.76503525805559,'Driver_24','123456',NULL),(25,'Car number 25','',1,0,106.68097398368278,10.762340450360082,'Driver_25','123456',NULL),(26,'Car number 26','',2,0,106.68164322438383,10.76096655710002,'Driver_26','123456',NULL),(27,'Car number 27','',2,0,106.68431557179244,10.762468177945738,'Driver_27','123456',NULL),(28,'Car number 28','',1,0,106.6844264582679,10.760379976349611,'Driver_28','123456',NULL),(29,'Car number 29','',1,0,106.685664683772,10.761559216640512,'Driver_29','123456',NULL),(30,'Car number 30','',2,0,106.68261196022097,10.76256676983973,'Driver_30','123456',NULL);
/*!40000 ALTER TABLE `car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerinfor`
--

DROP TABLE IF EXISTS `customerinfor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerinfor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerName` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `lng` double NOT NULL DEFAULT '0',
  `lat` double NOT NULL DEFAULT '0',
  `phoneNumber` varchar(45) CHARACTER SET latin1 NOT NULL,
  `carID` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `address` varchar(200) DEFAULT NULL,
  `adminActor` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `regDate` bigint(20) DEFAULT '0',
  `carType` int(11) DEFAULT NULL,
  `rejectCount` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerinfor`
--

LOCK TABLES `customerinfor` WRITE;
/*!40000 ALTER TABLE `customerinfor` DISABLE KEYS */;
INSERT INTO `customerinfor` VALUES (1,'nghiatn222',106.6825692,10.7626772,'0099912312',2,2,'Quận 5 Sài Gòn','namnnp','Cô 3 Sài Gòn',1510938455828,1,0),(2,'qweq',0,0,'0099912312',0,0,'123','','sdad',1510939142288,2,0),(3,'aaaaaaa',0,0,'123123',0,0,'b bb','','eeee',1511087822752,2,0),(4,'aaaaaaa',0,0,'123123',0,0,'b bb','','eeee',1511087832556,1,0),(5,'Nghĩa TN',106.6777534,10.7645599,'0968399506',0,0,'Hẻm 52 Hồ Thị Kỷ, phường 1, Quận 10, Hồ Chí Minh, Vietnam','','đến đón gấp',1511323936394,2,0),(6,'Nghĩa TN',0,0,'0968399506',0,0,'Q5 NVC 123 nhé','','đến đón gấp',1511324207342,2,0),(7,'nghĩa',106.6825692,10.7626772,'009999222000',0,1,'227 Nguyễn Văn Cừ, phường 4, Hồ Chí Minh, Vietnam','namnnp','đón gấp, đang đẻ',1511325247707,2,0),(8,'ca sad',0,0,'66666',0,0,'sadas ','','sad',1511528107116,2,0),(9,'ca sad',0,0,'66666',0,0,'sadas  sda','','sad',1511528357282,2,0),(10,'das ',0,0,'232312314',0,0,'das dasd1wd','','dsd',1511529161507,2,0),(11,'das ',0,0,'232312314',0,1,'das dasd1wd','namnnp','dsd',1511529256432,2,0),(12,'das ',0,0,'232312314',0,1,'das dasd1wd','namnnp','dsd',1511529268287,2,0),(13,'das ',106.6633746,10.7540279,'232312314',0,0,'District 5, Ho Chi Minh, Vietnam','','dsd',1511529292805,2,0),(14,'das ',106.685964,10.76717,'232312314',1,2,'189A/54 Cống Quỳnh, Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh, Vietnam','namnnp','dsd',1511529299806,2,0);
/*!40000 ALTER TABLE `customerinfor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,1,'Operator Admin'),(2,2,'Locator Admin'),(3,3,'Observer Admin');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-09 10:52:29
