package server;

import org.apache.log4j.Logger;

public class ShutdownThread extends Thread {

    private final JettyServer httpServer;
    private static final Logger logger = Logger.getLogger(ShutdownThread.class);

    public ShutdownThread(JettyServer httpServer) {
        this.httpServer = httpServer;
    }

    @Override
    public void run() {
        logger.info("BARG Server Waiting for shutting down.........");
        try {
            httpServer.shutdown();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }

        logger.info("BARG Server shutted down!");
    }

}
