/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.http;

import javax.servlet.http.HttpServletRequest;

public interface IServletRequestHandler {

    public String handle(HttpServletRequest req);
}
