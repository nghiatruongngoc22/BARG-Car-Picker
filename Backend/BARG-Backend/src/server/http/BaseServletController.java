package server.http;

import api.factory.HandlerFactoryManagement;
import api.factory.IHandlerFactory;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import server.main.Main;
import utils.GsonUtils;

public class BaseServletController extends HttpServlet {

    private static final long serialVersionUID = 1694368196592684215L;
    private static final Logger logger = Logger.getLogger(BaseServletController.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        this.doProcess(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        this.doProcess(req, resp);
    }

    private void doProcess(HttpServletRequest req, HttpServletResponse resp) {
        resp.setContentType("application/json;charset=UTF-8");
        try {
            String result = this.doHandle(req, resp);
            this.outAndClose(result, req, resp);
        } catch (Exception ex) {
            logger.error("doHandle exception " + ex.getMessage(), ex);
        }

    }

    private String doHandle(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        String pathName = "", methodName = "", result = "";
        List<String> params = null;
        try {
            params = this.parseUriRequest(req);
            System.out.println("params: " + GsonUtils.toJsonString(params));
            if (params.size() < Main.maxPathLenght) {
                return ("Invalid path: " + req.getPathInfo());
            }

            pathName = params.get(Main.pathNameIndex);
            methodName = params.get(Main.methodNameIndex);
            IHandlerFactory factory = HandlerFactoryManagement.getFactory(pathName);
            if (factory == null) {
                return ("Invalid path: " + pathName);
            }

            BaseServletHandler handler = factory.getHandler(methodName);
            if (handler == null) {
                return ("Invalid method: " + methodName);
            }
            resp.setContentType(handler.getContentType());
            result = handler.handle(req);
            return result;

        } finally {
            // Send queue to save systemapilog
        }

    }

    protected List<String> parseUriRequest(HttpServletRequest req) {
        List<String> result = new ArrayList();
        try {
            String uripath = req.getRequestURI();
            String[] splitArray = uripath.split("/");
            for (int i = 1; i < splitArray.length; i++) {
                result.add(splitArray[i]);
            }
        } catch (Exception ex) {
            logger.error("parseUriRequest exception " + ex.getMessage(), ex);
        }
        return result;
    }

    protected void outAndClose(String content, HttpServletRequest req, HttpServletResponse resp) {

        PrintWriter out = null;
        try {
            resp.setCharacterEncoding("UTF-8");
            out = resp.getWriter();
            out.print(content);
        } catch (Exception ex) {
            logger.error("outAndClose exception " + ex.getMessage(), ex);
            logger.error("requestURI=" + req.getRequestURI() + "?" + req.getQueryString());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
