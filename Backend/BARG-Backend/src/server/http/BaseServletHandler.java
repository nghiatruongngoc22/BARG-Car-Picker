package server.http;

public abstract class BaseServletHandler implements IServletRequestHandler {

    public static final String HEADER_JSON = "application/json;charset=UTF-8";
    public static final String HEADER_HTML = "text/html; charset=utf-8";

    public String getContentType() {
        return HEADER_JSON;
    }
}
