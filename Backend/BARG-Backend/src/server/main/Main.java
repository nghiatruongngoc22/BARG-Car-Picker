package server.main;

import api.common.CronJobFactory;
import api.config.Config;
import api.config.InitFileConfig;
import api.dao.CarDAO;
import api.dao.pool.PoolSettingManager;
import api.entity.CustomerInforEntity;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import server.JettyServer;
import server.ShutdownThread;
import static utils.FirebaseUtils.initFirebase;
import utils.LogUtils;

public class Main {

    public static int maxPathLenght = 2;
    public static int pathNameIndex = 0;
    public static int methodNameIndex = 1;

    public static void main(String[] args) throws Exception {

        try {
            LogUtils.init();

            initBusiness();

            initFirebase();
            
            initCronJob();

            final Logger logger = Logger.getLogger("Main");

            if (!PoolSettingManager.loadBargDB()) {
                throw new Exception("load setting db config fail");
            }

            if ("development".equals(System.getProperty("appenv")) == false) {
                System.out.close();
                System.err.close();
            }

            CarDAO carDAO = new CarDAO();

            carDAO.truncateCar();
            carDAO.generateCar(InitFileConfig.genCarLatLngCenter,
                    InitFileConfig.genCarRadius,
                    InitFileConfig.carNum);

            logger.info("Generate Cars " + new Date() + " successfully...");

            JettyServer webserver = new JettyServer();
            webserver.start();

            logger.info("BARG Server started at " + new Date() + " successfully...");

            ShutdownThread obj = new ShutdownThread(webserver);
            Runtime.getRuntime().addShutdownHook(obj);

        } catch (Throwable e) {
            Logger.getLogger(Main.class).error("Exception at start up : " + e.getMessage(), e);
            System.exit(3);
        }
    }

    public static void initBusiness() {
        Config.initPath();
        InitFileConfig.loadConfigs();
    }
    
    
    public static void initCronJob() throws Exception {

        if (InitFileConfig.cronJobStatusCheckCarNotfound) {
            Scheduler scheduler = CronJobFactory.getScheduleRefundVoucherInstance();
            if (scheduler != null) {
                scheduler.start();
                 Logger.getLogger(Main.class).info("Main-initCronJob: CronJob Refund Voucher ready");
            } else {
                 Logger.getLogger(Main.class).info("Main-initCronJob: Init CronJob Refund Voucher failed");
            }
        }
    }
}
