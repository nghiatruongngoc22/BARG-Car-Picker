package server;

import api.config.InitFileConfig;
import javax.servlet.Filter;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.FilterMapping;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlets.GzipFilter;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.servlet.ServletHolder;
import server.http.BaseServletController;

public class JettyServer extends Thread {

    private static final Logger logger = Logger.getLogger(JettyServer.class);

    private Server server;
    
    @Override
    public void run() {
        try {
            this.startWebServer();
        } catch (Exception ex) {
            logger.error("BARG Web server error", ex);
        }
    }

    public void startWebServer() throws Exception {
        server = new Server();

        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setMinThreads(InitFileConfig.jettyMinThreads);
        threadPool.setMaxThreads(InitFileConfig.jettyMaxThreads);
        server.setThreadPool(threadPool);

        SelectChannelConnector connector = new SelectChannelConnector();
        connector.setPort(InitFileConfig.jettyListenPort);
        connector.setLowResourcesConnections(InitFileConfig.jettyLowResourcesConnections);
        connector.setAcceptors(InitFileConfig.jettyAcceptors);

        server.setConnectors(new Connector[]{
            connector
        });

        ServletHandler handler = new ServletHandler();

        //viewer servlet
        ServletHolder holderPub = handler.addServletWithMapping(BaseServletController.class, "/barg/*");
        holderPub.setAsyncSupported(true);
        FilterHolder gzipFilterHolder = this.createGzipFilterHolder();
        handler.addFilter(gzipFilterHolder, this.createFilterMapping("/*", gzipFilterHolder));

        server.setHandler(handler);
        server.setStopAtShutdown(true);
        server.setGracefulShutdown(1000);//1 giay se dong
        server.setSendServerVersion(false);
        
        server.start();
        logger.info("BARG API Web server started");
        server.join();

    }

    private FilterHolder createGzipFilterHolder() {
        Filter gzip = new GzipFilter();
        FilterHolder filterHolder = new FilterHolder(gzip);
        filterHolder.setName("gzip");
        return filterHolder;
    }

    private FilterMapping createFilterMapping(String pathSpec, FilterHolder filterHolder) {
        FilterMapping filterMapping = new FilterMapping();
        filterMapping.setPathSpec(pathSpec);
        filterMapping.setFilterName(filterHolder.getName());
        return filterMapping;
    }

    public void shutdown() throws Exception {
        logger.info("HttpServer Waiting for shut down.........");
        if (server != null) {
            server.stop();
        }
        logger.info("HttpServer shutted down!");
    }
}
