/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import api.config.InitFileConfig;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.net.URLEncoder;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import static server.main.Main.initBusiness;

public class ApiUtils {

    private static final Logger logger = Logger.getLogger("ApiUtils");

    public static String geoCodingGoogle(String address) throws Exception {
        String url = "";

        try {
            String googleKey = InitFileConfig.googleKey;
            url = InitFileConfig.geoCodingUrl;
            
            address = URLEncoder.encode(address, "UTF-8");

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder
                    .append(url).append("?")
                    .append("address").append("=")
                    .append(address).append("&")
                    .append("key").append(googleKey);
            
            logger.info("geoCodingGoogle request: url=" + stringBuilder.toString());
            String resultMsg = "";
            resultMsg = HttpUtils.sendGet(stringBuilder.toString(), InitFileConfig.googleAPITimeout);

            logger.info("geoCodingGoogle response: " + resultMsg);

            return resultMsg;

        } catch (Exception ex) {
            logger.error("geoCodingGoogle parse json : " + ex);
            return "";
        }
    }
    
    
    public static String reverseGeoCodingGoogle(double lat, double lng) throws Exception {
        String url = "";

        try {
            String googleKey = InitFileConfig.googleKey;
            url = InitFileConfig.geoCodingUrl;

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder
                    .append(url).append("?")
                    .append("latlng").append("=")
                    .append(lat).append(",")
                    .append(lng).append("&")
                    .append("key").append(googleKey);
            
            logger.info("reverseGeoCodingGoogle request: url=" + stringBuilder.toString());
            String resultMsg = "";
            resultMsg = HttpUtils.sendGet(stringBuilder.toString(), InitFileConfig.googleAPITimeout);

            logger.info("reverseGeoCodingGoogle response: " + resultMsg);

            return resultMsg;

        } catch (Exception ex) {
            logger.error("geoCodingGoogle parse json : " + ex);
            return "";
        }
    }
    
    public static String distanceGoogle(double originLat, double originLng, double desLat, double desLng) throws Exception {
        String url = "";

        try {
            String googleKey = InitFileConfig.googleKey;
            url = InitFileConfig.distanceUrl;

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder
                    .append(url).append("?")
                    .append("origins").append("=")
                    .append(originLat).append(",")
                    .append(originLng).append("&")
                    .append("destinations").append("=")
                    .append(desLat).append(",")
                    .append(desLng).append("&")
                    .append("key").append(googleKey);
            
            logger.info("distanceMatrixGoogle request: url=" + stringBuilder.toString());
            String resultMsg = "";
            resultMsg = HttpUtils.sendGet(stringBuilder.toString(), InitFileConfig.googleAPITimeout);

            logger.info("distanceMatrixGoogle response: " + resultMsg);

            return resultMsg;

        } catch (Exception ex) {
            logger.error("geoCodingGoogle parse json : " + ex);
            return "";
        }
    }
    
    
    public static void main(String args[]) throws Exception {
        LogUtils.init();

            initBusiness();
        
        String result = reverseGeoCodingGoogle(10.7645599, 106.6777534);

        
        
        System.out.println(result.toString());
    }
}
