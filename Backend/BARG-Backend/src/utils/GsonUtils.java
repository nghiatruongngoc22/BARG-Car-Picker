/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class GsonUtils {

    private static final Gson gson;

    static {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        gson = gsonBuilder.disableHtmlEscaping().create();
    }

    public static String toJsonString(Object obj) {
        return gson.toJson(obj);
    }

    public static <T> T fromJsonString(String sJson, Class<T> t) {
        return gson.fromJson(sJson, t);
    }

    public static <T> T json2Collection(String sJson, Type t) {
        return gson.fromJson(sJson, t);
    }

    public static void main(String[] args) {
        Map<Integer, Integer> data = new HashMap<>();
        data.put(1, 2);
        data.put(3, 4);

        String dataStr = "{\"1\":2,\"3\":4}";

//        String s = GsonUtils.toJsonString(data);
//        System.out.println(s);

        Type type = new TypeToken<Map<Integer, Integer>>() {}.getType();
        Map<Integer, Integer> resultMap
                = GsonUtils.json2Collection(dataStr, type);
        System.out.println(GsonUtils.toJsonString(resultMap));
    }
}
