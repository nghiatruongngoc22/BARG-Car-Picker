/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import api.entity.CustomerInforEntity;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author NghiaTruongNgoc
 */
public class FirebaseUtils {

    public static void initFirebase() throws IOException, FileNotFoundException {
        FileInputStream serviceAccount = new FileInputStream("./conf/serviceAccountKey-BARG.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://barg-car-picker.firebaseio.com/")
                .build();

        FirebaseApp otherApp = FirebaseApp.initializeApp(options);
        System.out.println(otherApp.getName() + "Firebase initialized");

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("");

        DatabaseReference usersRef = ref.child("customerinfo");

        //usersRef.removeValueAsync();
    }
    
    public static void updateCustomerInfo(CustomerInforEntity entity) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("");
        DatabaseReference usersRef = ref.child("customerinfo");
        
        usersRef.child(String.valueOf(entity.getId())).setValueAsync(entity);
    }
}
