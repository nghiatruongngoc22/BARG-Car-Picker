
package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

public class HttpUtils {

    private static final Logger logger = Logger.getLogger(HttpUtils.class.getSimpleName());

    private static String serverIP = null;

    public static String getServerIP() {
        if (serverIP == null) {
            try {
                Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
                while (n.hasMoreElements()) {
                    NetworkInterface e = n.nextElement();
                    Enumeration<InetAddress> a = e.getInetAddresses();
                    while (a.hasMoreElements()) {
                        InetAddress addr = a.nextElement();
                        if (addr.isLinkLocalAddress() == false && addr.isLoopbackAddress() == false && addr.isSiteLocalAddress() == true) {
                            serverIP = addr.getHostAddress();
                            return serverIP;
                        }
                    }
                }
            } catch (Exception ex) {
                logger.error("getServerIP exception " + ex.getMessage(), ex);
                try {
                    serverIP = InetAddress.getLocalHost().getHostName();
                } catch (UnknownHostException ex1) {
                    logger.error("getServerIP exception " + ex1.getMessage(), ex1);
                    serverIP = "127.0.0.1";
                }
            }
        }
        return serverIP;
    }

   

    public static String postParams(String url, Map<String, String> datas, int timeout) throws IOException {        
        RequestConfig.Builder builder = RequestConfig.custom()
                .setSocketTimeout(timeout)
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setStaleConnectionCheckEnabled(true);

        RequestConfig requestConfig = builder.build();

        try (CloseableHttpClient httpClient = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .build();) {
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");

            List<NameValuePair> nvps = new ArrayList<>();
            Iterator<String> dataKeys = datas.keySet().iterator();
            while (dataKeys.hasNext()) {
                String key = dataKeys.next();
                String value = datas.get(key);
                nvps.add(new BasicNameValuePair(key, value));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new IOException("Failed : HTTP getStatusCode: "
                            + response.getStatusLine().getStatusCode()
                            + " HTTP getReasonPhrase: "
                            + response.getStatusLine().getReasonPhrase());
                }
                HttpEntity entity = response.getEntity();
                try (InputStream inputStream = entity.getContent();) {
                    return IOUtils.toString(inputStream, "UTF-8");
                }
            }
        }
    }

    public static String getParameter(HttpServletRequest req, String paramName) {
        String value = req.getParameter(paramName);
        if (value == null) {
            value = "";
        }
        return value;
    }

    public static String getRequestUrl(HttpServletRequest request) {
        StringBuilder stringBuilder = new StringBuilder(request.getRequestURI());
        Enumeration enu = request.getParameterNames();
        stringBuilder.append("?");
        while (enu.hasMoreElements()) {
            String paramName = (String) enu.nextElement();
            stringBuilder.append(paramName).append("=")
                    .append(request.getParameter(paramName))
                    .append("&");
        }
        return stringBuilder.toString();
    }

    public static String getClientIP(HttpServletRequest request) {
        String ipAddress = "";
        try {
            ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            
            int i = ipAddress.indexOf(",");
            if (i > -1) {
                if (i > 0) {
                    ipAddress = ipAddress.substring(0, i).trim();
                } else {
                    ipAddress = ipAddress.substring(i + 1, ipAddress.length()).trim();
                }
            }
        } catch (Exception ex) {
            logger.error("getClientIP exception " + ex.getMessage(), ex);
        }
        return ipAddress;
    }

    public static String sendPost(List<NameValuePair> nvps, String postUrl, int timeout) throws UnsupportedEncodingException, IOException {
        String result;
        RequestConfig defaultRequestConfig = RequestConfig.custom()
                .setSocketTimeout(timeout)
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setStaleConnectionCheckEnabled(true)
                .build();
        try (CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build()) {
            HttpPost httpPost = new HttpPost(postUrl);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            try (CloseableHttpResponse response = httpclient.execute(httpPost)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new IOException("Failed : HTTP getStatusCode: "
                            + response.getStatusLine().getStatusCode() + " HTTP getReasonPhrase: " + response.getStatusLine().getReasonPhrase());
                }
                HttpEntity entity = response.getEntity();
                try (InputStream inputStream = entity.getContent()) {
                    result = IOUtils.toString(inputStream, "UTF-8");
                }
            }
        }
        return result;
    }

    public static String sendGet(String url, int timeout) throws IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom()
                .setSocketTimeout(timeout)
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setStaleConnectionCheckEnabled(true)
                .build();

        try (CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build()) {
            HttpGet request = new HttpGet(url);
            try (CloseableHttpResponse response = httpclient.execute(request)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new IOException("Failed : HTTP getStatusCode: "
                            + response.getStatusLine().getStatusCode()
                            + " HTTP getReasonPhrase: "
                            + response.getStatusLine().getReasonPhrase());
                }       
                try (BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))) {
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = rd.readLine()) != null) {
                        result.append(line);
                    }
                    return result.toString();
                }
            }
        }
    }

}
