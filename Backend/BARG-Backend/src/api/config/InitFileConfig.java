package api.config;

import static api.config.Config.getParam;
import api.entity.LatLng;
import org.apache.log4j.Logger;

public class InitFileConfig {

    private static final Logger logger = Logger.getLogger(InitFileConfig.class.getSimpleName());
    //jetty
    public static int jettyListenPort = 0;
    public static int jettyMinThreads = 0;
    public static int jettyMaxThreads = 0;
    public static int jettyAcceptors = 0;
    public static int jettyLowResourcesConnections = 0;
    public static int jettyLowResourcesMaxIdleTime = 0;
    public static String googleKey = "";
    public static String geoCodingUrl = "";
    public static String distanceUrl = "";
    public static int googleAPITimeout;
    
    public static LatLng genCarLatLngCenter;
    public static int genCarRadius;
    public static int carNum;
    public static int rejectCountCfg = 0;
    
    
    
    public static String cronTimeCheckCarNotfound = "";
    public static String jobIDCheckCarNotfound = "";
    public static String cronGroupIDCheckCarNotfound = "";
    public static String triggerIDCheckCarNotfound = "";
    public static boolean cronJobStatusCheckCarNotfound;
    public static int intervalTime;
    
    public static boolean loadConfigs() {
        boolean result = false;
        try {
            logger.info("Start load file config!");
            //section [jetty]
            jettyListenPort = Integer.valueOf(getParam("jetty", "listenPort"));
            jettyMinThreads = Integer.valueOf(getParam("jetty", "minThreads"));
            jettyMaxThreads = Integer.valueOf(getParam("jetty", "maxThreads"));
            jettyAcceptors = Integer.valueOf(getParam("jetty", "acceptors"));
            jettyLowResourcesConnections = Integer.valueOf(getParam("jetty", "lowResourcesConnections"));
            jettyLowResourcesMaxIdleTime = Integer.valueOf(getParam("jetty", "lowResourcesMaxIdleTime"));
            googleKey = getParam("GoogleAPI", "key");
            geoCodingUrl = getParam("GoogleAPI", "geoCodingUrl");
            distanceUrl = getParam("GoogleAPI", "distanceUrl");
            googleAPITimeout = Integer.valueOf(getParam("GoogleAPI", "timeout"));
            
            double lat = Double.parseDouble(getParam("mock", "lat"));
            double lng = Double.parseDouble(getParam("mock", "lng"));
            
            genCarLatLngCenter = new LatLng(lat, lng);
            
            genCarRadius = Integer.valueOf(getParam("mock", "radiusGenCar"));
            
            carNum = Integer.valueOf(getParam("mock", "carNum"));
            
            rejectCountCfg = Integer.valueOf(getParam("mock", "rejectCount"));
            
            cronTimeCheckCarNotfound = getParam("cronJobCheckCarNotfound", "cronTime");
            jobIDCheckCarNotfound = getParam("cronJobCheckCarNotfound", "jobID");
            cronGroupIDCheckCarNotfound = getParam("cronJobCheckCarNotfound", "groupID");
            triggerIDCheckCarNotfound = getParam("cronJobCheckCarNotfound", "triggerID");
            cronJobStatusCheckCarNotfound = "1".equals(getParam("cronJobCheckCarNotfound", "status"));
            
            intervalTime = Integer.valueOf(getParam("cronJobCheckCarNotfound", "intervalTime"));
            
            logger.info("Load file config success!");
        } catch (Exception ex) {
            logger.error("loadConfigs exception " + ex.getMessage(), ex);
        }
        return result;
    }
}
