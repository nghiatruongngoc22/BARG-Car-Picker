/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.common;

import api.config.InitFileConfig;
import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;


/**
 *
 * @author nghiatn
 */
public class CronJobFactory {

    private static SchedulerFactory schedulerFactory;
    private static Scheduler scheduleCheckCarNotFound = null;
    
    private static final Logger logger = Logger.getLogger("RefundVoucherBuz");
    public static Scheduler getScheduleRefundVoucherInstance() {
        if (scheduleCheckCarNotFound == null) {
            schedulerFactory = new StdSchedulerFactory();
            try {
                scheduleCheckCarNotFound = schedulerFactory.getScheduler();
                initJobCheckCarNotfound();
            } catch (Exception ex) {
                logger.error("CronJobFactory-getScheduleRefundVoucherInstance-ERROR: " + ex.getMessage(), ex);
                scheduleCheckCarNotFound = null;
            }
        }

        return scheduleCheckCarNotFound;
    }
    
    
    private static void initJobCheckCarNotfound() throws Exception {
        String jobID = InitFileConfig.jobIDCheckCarNotfound;
        String groupID = InitFileConfig.cronGroupIDCheckCarNotfound;
        String cronTime = InitFileConfig.cronTimeCheckCarNotfound;
        String triggerID = InitFileConfig.triggerIDCheckCarNotfound;
        
        JobDetail job = JobBuilder.newJob(CheckCarNotFoundJob.class)
                .withIdentity(jobID, groupID)
                .build();

        CronTrigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(triggerID, groupID).startNow()
                .withSchedule(CronScheduleBuilder.cronSchedule(cronTime))
                .build();
        try {
            scheduleCheckCarNotFound.scheduleJob(job, trigger);
        } catch (Exception ex) {
            logger.error("CronJobFactory-initJobCheckCarNotfound-ERROR: " + ex.getMessage(), ex);
        }
    }
    
    
}
