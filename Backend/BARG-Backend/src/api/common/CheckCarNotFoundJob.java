/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.common;

import api.dao.CustomerInforDAO;
import api.entity.CustomerInforEntity;
import api.enums.APIStatusEnum;
import api.enums.CustomerInforStatus;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import utils.FirebaseUtils;

/**
 *
 * @author nghiatn
 */
public class CheckCarNotFoundJob implements Job {

    private static final Logger logger = Logger.getLogger("JobRefund");

    public CheckCarNotFoundJob() {
        //dump constructor
    }

    @Override
    public void execute(JobExecutionContext context) {
        JobKey jobKey = context.getJobDetail().getKey();
        System.out.println("CheckCarNotFoundJob" + jobKey + " is executing at" + new Date()
                + "\n==================================================================================");

        try {
            CustomerInforDAO customerDao = new CustomerInforDAO();
            List<CustomerInforEntity> infos = customerDao.getExpiredCustomerInfo();

            if (infos == null) {
                return;
            }

            for (CustomerInforEntity entity : infos) {
                entity.setStatus(CustomerInforStatus.CAR_NOT_FOUND.getValue());
                entity.setAdminActor("JobServer");
                customerDao.markStatus(entity.getId(), CustomerInforStatus.CAR_NOT_FOUND.getValue(), "JobServer");
                FirebaseUtils.updateCustomerInfo(entity);
            }

        } catch (Exception ex) {
            logger.error("CheckCarNotFoundJob-execute-ERROR: " + ex.getMessage(), ex);
        }
    }
}
