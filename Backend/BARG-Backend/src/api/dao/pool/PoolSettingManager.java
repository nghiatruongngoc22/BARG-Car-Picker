/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao.pool;

import api.dao.shared.DataSourceEnum;
import api.dao.shared.DataSourceWrapper;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

public class PoolSettingManager {

    public static final Logger logger = Logger.getLogger(PoolSettingManager.class);

    public static PoolSetting bargPoolDB = null;
    
    public static DataSourceWrapper bargWrapper;

    public static DataSourceWrapper getDataSource(DataSourceEnum dataSourceEnum) {

        switch (dataSourceEnum) {
            case BARG_DB:
                return bargWrapper;
        }
        return null;
    }

    public static boolean loadBargDB() throws Exception {
        bargPoolDB = new PoolSetting(PoolSetting.BARG_DB_NAME);
        
        return initDataSource();
    }
    
    public static boolean initDataSource() {
        try {
            if (bargPoolDB != null && bargPoolDB.isEnableExternalPoolLib()) {
                bargWrapper = new DataSourceWrapper(bargPoolDB);
            }
            
            return true;
        } catch (Exception ex) {
            logger.error(ExceptionUtils.getMessage(ex), ex);
        }
        return false;
    }

}
