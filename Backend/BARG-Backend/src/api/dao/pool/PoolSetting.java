/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao.pool;

import api.config.Config;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class PoolSetting {

    private static final Logger logger = Logger.getLogger(PoolSetting.class);

    public static final String BARG_DB_NAME = "bargDB";


    private String driver = "";
    private String url = "";
    private String user = "";
    private String password = "";
    private int poolSize = 20;
    private boolean cachePrepStmts = true;
    private int prepStmtCacheSize = 250;
    private int prepStmtCacheSqlLimit = 2048;
    private boolean enableExternalPoolLib = false;

    public PoolSetting(String driver, String url, String user, String password, int poolSize) {

        this.driver = driver;
        this.url = url;
        this.user = user;
        this.password = password;
        this.poolSize = poolSize;
    }

    public PoolSetting(String dbName) throws Exception {
        driver = Config.getParam(dbName, "driver");
        String urlConfig = Config.getParam(dbName, "url");
        if (urlConfig != null && urlConfig.lastIndexOf("&") != urlConfig.length() - 1) {
            urlConfig += "&";
        }
        url = urlConfig;
        user = Config.getParam(dbName, "user");
        password = Config.getParam(dbName, "password");
        String pSize = Config.getParam(dbName, "poolSize");
        if (pSize != null && !pSize.isEmpty()) {
            poolSize = Integer.parseInt(pSize);
        }

        String pCachePrepStmts = Config.getParam(dbName, "cachePrepStmts");
        if (StringUtils.isNotBlank(pCachePrepStmts)) {
            cachePrepStmts = Boolean.parseBoolean(pCachePrepStmts);
        }

        String pPrepStmtCacheSize = Config.getParam(dbName, "prepStmtCacheSize");
        if (StringUtils.isNotBlank(pPrepStmtCacheSize)) {
            prepStmtCacheSize = Integer.parseInt(pPrepStmtCacheSize);
        }

        String pPrepStmtCacheSqlLimit = Config.getParam(dbName, "prepStmtCacheSqlLimit");
        if (StringUtils.isNotBlank(pPrepStmtCacheSqlLimit)) {
            prepStmtCacheSqlLimit = Integer.parseInt(pPrepStmtCacheSqlLimit);
        }

        String pEnableExternalPoolLib = Config.getParam(dbName, "enableExternalPoolLib");
        if (StringUtils.isNotBlank(pEnableExternalPoolLib)) {
            enableExternalPoolLib = Boolean.parseBoolean(pEnableExternalPoolLib);
        }
    }

    /**
     * @return the driver
     */
    public String getDriver() {
        return driver;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return the poolSize
     */
    public int getPoolSize() {
        return poolSize;
    }

    /**
     * @return the cachePrepStmts
     */
    public boolean isCachePrepStmts() {
        return cachePrepStmts;
    }

    /**
     * @return the prepStmtCacheSize
     */
    public int getPrepStmtCacheSize() {
        return prepStmtCacheSize;
    }

    /**
     * @return the prepStmtCacheSqlLimit
     */
    public int getPrepStmtCacheSqlLimit() {
        return prepStmtCacheSqlLimit;
    }

    /**
     * @return the enableExternalPoolLib
     */
    public boolean isEnableExternalPoolLib() {
        return enableExternalPoolLib;
    }

    public void setEnableExternalPoolLib(boolean enableExternalPoolLib) {
        this.enableExternalPoolLib = enableExternalPoolLib;
    }

}
