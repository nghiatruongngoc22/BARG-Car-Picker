/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao;

import api.config.InitFileConfig;
import api.constant.TableNameConstants;
import api.dao.pool.PoolSettingManager;
import api.dao.shared.ConditionClause;
import api.entity.CarEntity;
import api.entity.GetCarLocationResponse;
import api.entity.LatLng;
import api.enums.APIStatusEnum;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.*;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import static server.main.Main.initBusiness;
import utils.ApiUtils;
import java.security.NoSuchAlgorithmException;
import static utils.ApiUtils.reverseGeoCodingGoogle;
import utils.HashUtils;
import utils.LogUtils;

public class CarDAO extends BargDAO<CarEntity> {

    public CarDAO() {
        setTableName(TableNameConstants.CAR_TABLE);
    }
    
    public boolean checkCarSig(String username, String sig) {
        CarEntity entity = getCarByDriverName(username);
        
        if (entity != null && entity.getSig().equals(sig)) {
            return true;
        }
        
        return false;
    }
    

    @Override
    protected CarEntity mapFromResultSetToEntity(int index, ResultSet rs) throws Exception {
        final CarEntity entity = new CarEntity();

        entity.setDescription(rs.getString("description"));
        entity.setPickAddress(rs.getString("pickAddress"));
        entity.setType(rs.getInt("type"));
        entity.setIsBusy(rs.getInt("isBusy"));
        entity.setLat(rs.getDouble("lat"));
        entity.setLng(rs.getDouble("lng"));
        entity.setId(rs.getInt("id"));
        entity.setDriverName(rs.getString("driverAccountName"));
        entity.setSig(rs.getString("sig"));
        entity.setPassword(rs.getString("password"));

        return entity;
    }

    public int insert(CarEntity entity) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();

        values.put("description", entity.getDescription());
        values.put("pickAddress", entity.getPickAddress());
        values.put("type", entity.getType());
        values.put("isBusy", entity.getIsBusy());
        values.put("lat", entity.getLat());
        values.put("lng", entity.getLng());
        values.put("password", entity.getPassword());
        values.put("driverAccountName", entity.getDriverName());

        return insert(values);
    }

    public int updateCar(int id, int status, String address) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();

        values.put("isBusy", status);
        values.put("pickAddress", address);

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("id", id);

        return update(values, conditionClause);
    }

    public int truncateCar() throws Exception {
        String query = "TRUNCATE " + getTableName();
        return delete(query);
    }

    public boolean generateCar(LatLng centerLatLng, int maxRadius, int carNum) throws Exception {
        List<CarEntity> carList = new ArrayList<>();

        //This is to generate carNum random points
        for (int i = 0; i < carNum; i++) {

            double x0 = centerLatLng.latitude;
            double y0 = centerLatLng.longitude;

            int radius = 50 + (int) (Math.random() * ((maxRadius - 50) + 1));

            Random random = new Random();

            // Convert radius from meters to degrees
            double radiusInDegrees = radius / 111000f;

            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west distances
            double new_x = x / Math.cos(y0);

            double foundLatitude = new_x + x0;
            double foundLongitude = y + y0;
            LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);

            int type = 1 + (int) (Math.random() * ((2 - 1) + 1));

            CarEntity car = new CarEntity();
            car.setDescription("Car number " + (i + 1));
            car.setType(type);
            car.setLat(randomLatLng.latitude);
            car.setLng(randomLatLng.longitude);
            car.setDriverName("Driver" + (i+1));
            car.setPassword("123456");

            carList.add(car);
        }

        for (CarEntity car : carList) {
            insert(car);
        }

        return true;
    }

    public List<CarEntity> getAvailableCar(int carType) {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(TableNameConstants.CAR_TABLE);
        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("isBusy", 0);
        conditionClause.pushAnd();
        conditionClause.pushConditionEqual("type", carType);
        try {
            return queryGetMany(statement.toString(), conditionClause);
        } catch (Exception ex) {
            return null;
        }
    }

    public CarEntity getCarByID(int id) {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(TableNameConstants.CAR_TABLE);
        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("id", id);
        try {
            return queryGetOne(statement.toString(), conditionClause);
        } catch (Exception ex) {
            return null;
        }
    }

    public CarEntity getCarByDriverName(String drivename) {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(getTableName());
        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("driverAccountName", drivename);
        try {
            return queryGetOne(statement.toString(), conditionClause);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public String updateCarSig(String username, String pasword) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();

        String carSig = buildCarSig(username, pasword);

        values.put("sig", carSig);

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("driverAccountName", username);

        update(values, conditionClause);

        return carSig;
    }

    private String buildCarSig(String username, String password) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append(username).append("|")
                .append(password).append("|")
                .append(System.currentTimeMillis());

        try {
            return HashUtils.hashSHA256(stringBuilder.toString());
        } catch (NoSuchAlgorithmException ex) {
            java.util.logging.Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    public CarEntity login(String username, String password) throws Exception {
        CarEntity entity = getCarByDriverName(username);

        if (entity != null && validatePassword(entity.getPassword(), password)) {
            String carSig = updateCarSig(username, password);
            entity.setSig(carSig);
            return entity;
        } else {
            return null;
        }
    }
    
    private boolean validatePassword(String password, String passwordReq) {
        return password.equals(passwordReq);
    }

    public List<CarEntity> getAvailableCarByRadius(int carType, int radius, double centerLat, double centerLng) {
        List<CarEntity> carListAll = getAvailableCar(carType);

        //loc ra truoc 10 xe trong ban kinh X (met)
        Iterator<CarEntity> iterator = carListAll.iterator();
        while (iterator.hasNext()) {
            CarEntity value = (CarEntity) iterator.next();

            if (distance(value.getLat(), value.getLng(), centerLat, centerLng) > radius) {
                iterator.remove();
            }
        }

        //filter car for get distance theo dung duong di
        List<CarEntity> returnCarList = new ArrayList<>();
        Iterator<CarEntity> iteratorRemain = carListAll.iterator();
        while (iteratorRemain.hasNext() && returnCarList.size() <= 3) {
            try {
                CarEntity car = (CarEntity) iteratorRemain.next();
                String resultMsgGoogle = ApiUtils.distanceGoogle(centerLat, centerLng, car.getLat(), car.getLng());
                double distance = distanceGoogle(resultMsgGoogle);
                if (distance == -1 || distance > radius) {
                    iteratorRemain.remove();
                } else {
                    car.setDistance(distance);
                    returnCarList.add(car);
                }
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ////

        return returnCarList;
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;

        return (dist) * 1000;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
 /*::  This function converts radians to decimal degrees             :*/
 /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private double distanceGoogle(String resultMsgGoogle) throws JSONException {
        double distance = -1;
        JSONObject jObj = new JSONObject(resultMsgGoogle);

        if ("OK".equals(jObj.getString("status"))) {
            JSONArray rows = jObj.getJSONArray("rows");
            distance = rows.getJSONObject(0).getJSONArray("elements").
                    getJSONObject(0).getJSONObject("distance").getDouble("value");
        }
        return distance;
    }

    public static void main(String args[]) throws Exception {
        LogUtils.init();

        initBusiness();
        if (!PoolSettingManager.loadBargDB()) {
            throw new Exception("load setting db config fail");
        }

        new CarDAO().truncateCar();
    }

    public boolean updateCarLocation(int id, double lat, double lng) {
        CarEntity entity = getCarByID(id);
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();

        if (entity == null) {
            return false;
        }

        values.put("lat", lat);
        values.put("lng", lng);

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("id", id);

        try {
            update(values, conditionClause);
            return true;
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CarDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean logout(String username, String sig) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();
        
        if (!checkCarSig(username, sig)) {
            return false;
        }
        
        values.put("sig", "");

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("driverAccountName", username);

        update(values, conditionClause);
        
        return true;
    }

    public boolean handleGetCarLocation(int id, GetCarLocationResponse response) {
        CarEntity entity = getCarByID(id);

        if (entity == null) {
            return false;
        }

        response.setLat(entity.getLat());
        response.setLng(entity.getLng());
        return true;
    }
}
