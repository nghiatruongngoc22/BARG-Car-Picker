/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao.shared;

import java.util.ArrayList;
import java.util.List;

public class BatchQuery {
    public List<QueryStatement> queries;

    public BatchQuery() {
        this.queries = new ArrayList<>();
    }

    public void pushQuery(QueryStatement statement) {
        queries.add(statement);
    }

    public void pushAllQueries(List<QueryStatement> statements) {
        queries.addAll(statements);
    }
}
