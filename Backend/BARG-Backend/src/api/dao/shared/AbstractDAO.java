/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao.shared;

import api.dao.pool.PoolSettingManager;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import utils.DateUtils;


public abstract class AbstractDAO<T> {

    public static final Logger LOGGER = Logger.getLogger("AbstractDAO");
    //protected DatabaseConfigEntity configEntity = null;
    protected String tableName = "";
    protected DataSourceEnum dataSourceEnum = null;

    protected String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public AbstractDAO() {
    }

    public AbstractDAO(DataSourceEnum _dataSourceEnum) {
        this.dataSourceEnum = _dataSourceEnum;
    }

    public String getPartition() {
        return DateUtils.date2Str(new java.util.Date(), "YYYYMM");
    }

    public int delete(String query) throws SQLException, Exception {
        //final StorageMySQL storageMySQL = getStorage();
        final Connection connection = getConnection();
        final PreparedStatement statement = connection.prepareStatement(query.toString());
        LOGGER.info("AbstractDAO-delete-INFOR: " + statement.toString());
        try {
            return statement.executeUpdate();
        } finally {
            releaseResource(connection, statement);
        }
    }

    public List<T> findAll() throws Exception {
        final List<T> results = new ArrayList<>();
        final Connection connection = getConnection();
        final PreparedStatement statement = connection.prepareStatement(selectAllQuery());
        LOGGER.info("AbstractDAO-findAll-INFOR: " + statement.toString());
        final ResultSet resultSet = statement.executeQuery();
        try {
            int index = 1;
            while (resultSet.next()) {
                results.add(mapFromResultSetToEntity(index++, resultSet));
            }
            return results;
        } finally {
            releaseResource(connection, statement, resultSet);
        }
    }

    public List<T> findAllBySql(String sql) throws Exception {
        final List<T> results = new ArrayList<>();
        final Connection connection = getConnection();
        final PreparedStatement statement = connection.prepareStatement(sql);
        LOGGER.info("AbstractDAO-findAllBySql-INFOR: " + statement.toString());
        final ResultSet resultSet = statement.executeQuery();
        try {
            int index = 1;
            while (resultSet.next()) {
                results.add(mapFromResultSetToEntity(index++, resultSet));
            }
            return results;
        } finally {
            releaseResource(connection, statement, resultSet);
        }
    }

    public List<T> getAllOrderByExpression(String orderByExpression) throws Exception {
        final List<T> results = new ArrayList<>();
        final Connection connection = getConnection();
        final PreparedStatement statement = connection.prepareStatement(selectAllQuery() + orderByExpression);
        LOGGER.info("AbstractDAO-getAllOrderByExpression-INFOR: " + statement.toString());
        final ResultSet resultSet = statement.executeQuery();
        try {
            int index = 1;
            while (resultSet.next()) {
                results.add(mapFromResultSetToEntity(index++, resultSet));
            }
            return results;
        } finally {
            releaseResource(connection, statement, resultSet);
        }
    }

    protected List<T> findByCondition(String query, ConditionClause conditionClause) throws Exception {
        final List<T> results = new ArrayList<>();
        final Connection connection = getConnection();
        query += conditionClause.getConditions();
        final PreparedStatement statement = connection.prepareStatement(query);
        prepareConditionParameters(conditionClause, statement);
        LOGGER.info("AbstractDAO-findByCondition-INFOR: " + statement.toString());
        final ResultSet resultSet = statement.executeQuery();
        try {
            int index = 1;
            while (resultSet.next()) {
                results.add(mapFromResultSetToEntity(index++, resultSet));
            }
        } finally {
            releaseResource(connection, statement, resultSet);
        }
        return results;
    }

    protected int countAll() throws Exception {
        final StringBuilder query = new StringBuilder("select count(*) as value from ").append(getTableName());
        final Connection connection = getConnection();
        final PreparedStatement statement = connection.prepareStatement(query.toString());
        LOGGER.info("AbstractDAO-countAll-INFOR: " + statement.toString());
        final ResultSet resultSet = statement.executeQuery();
        try {
            while (resultSet.next()) {
                return resultSet.getInt("value");
            }
        } finally {
            releaseResource(connection, statement, resultSet);
        }
        return 0;
    }

    protected int countByCondition(String query, ConditionClause conditionClause) throws Exception {
        final Connection connection = getConnection();
        query += conditionClause.getConditions();
        final PreparedStatement statement = connection.prepareStatement(query);
        prepareConditionParameters(conditionClause, statement);
        LOGGER.info("AbstractDAO-countByCondition-INFOR: " + statement.toString());
        final ResultSet resultSet = statement.executeQuery();
        try {
            while (resultSet.next()) {
                return resultSet.getInt("value");
            }
        } finally {
            releaseResource(connection, statement, resultSet);
        }
        return 0;
    }

    protected List<T> queryGetMany(String query, ConditionClause conditionClause) throws Exception {
        final List<T> results = new ArrayList<>();
        final Connection connection = getConnection();
        if (conditionClause != null && !StringUtils.isEmpty(conditionClause.getConditions())) {
            query += conditionClause.getConditions();
        }
        final PreparedStatement statement = connection.prepareStatement(query);
        prepareConditionParameters(conditionClause, statement);
        LOGGER.info("AbstractDAO-queryGetMany-INFOR: " + statement.toString());
        final ResultSet resultSet = statement.executeQuery();
        try {
            int index = 1;
            while (resultSet.next()) {
                results.add(mapFromResultSetToEntity(index++, resultSet));
            }
            return results;
        } finally {
            releaseResource(connection, statement, resultSet);
        }
    }

    protected List<T> queryGetMany(String query, ConditionClause conditionClause, String orderByExpression) throws Exception {
        final List<T> results = new ArrayList<>();
        final Connection connection = getConnection();
        if (conditionClause != null && !StringUtils.isEmpty(conditionClause.getConditions())) {
            query += conditionClause.getConditions() + " " + orderByExpression;
        }
        final PreparedStatement statement = connection.prepareStatement(query);
        prepareConditionParameters(conditionClause, statement);
        LOGGER.info("AbstractDAO-queryGetMany-INFOR: " + statement.toString());
        final ResultSet resultSet = statement.executeQuery();
        try {
            int index = 1;
            while (resultSet.next()) {
                results.add(mapFromResultSetToEntity(index++, resultSet));
            }
            return results;
        } finally {
            releaseResource(connection, statement, resultSet);
        }
    }

    protected int insert(LinkedHashMap<String, Object> values) throws Exception {
        final Connection connection = getConnection();
        final StringBuilder query = new StringBuilder("INSERT INTO\n").append(getTableName()).append("(\n");
        int index = 1;
        for (Map.Entry<String, Object> entry : values.entrySet()) {
            query.append(entry.getKey());
            if (index < values.size()) {
                query.append(",");
            }
            index++;
        }
        query.append(")VALUES(");
        index = 1;
        for (Map.Entry<String, Object> entry : values.entrySet()) {
            query.append("?");
            if (index < values.size()) {
                query.append(",");
            }
            index++;
        }
        query.append(")");
        final PreparedStatement statement = connection.prepareStatement(query.toString(), Statement.RETURN_GENERATED_KEYS);
        putStatementParameters(values, statement);
        LOGGER.info("AbstractDAO-insert-INFOR: " + statement.toString());
        try {
            int id = statement.executeUpdate();
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = (generatedKeys.getInt(1));
                }
            }
            return id;
        } finally {
            releaseResource(connection, statement);
        }
    }

    protected int update(LinkedHashMap<String, Object> values, ConditionClause conditionClause) throws Exception {
        final Connection connection = getConnection();
        final StringBuilder query = new StringBuilder("UPDATE\n").append(getTableName()).append("\nSET\n");
        int index = 1;
        for (Map.Entry<String, Object> entry : values.entrySet()) {
            query.append(entry.getKey()).append("=?");
            if (index < values.size()) {
                query.append(",");
            }
            index++;
        }
        if (conditionClause != null && !StringUtils.isEmpty(conditionClause.getConditions())) {
            query.append(conditionClause.getConditions());
        }
        final PreparedStatement statement = connection.prepareStatement(query.toString());
        putStatementParameters(values, statement);
        prepareConditionParameters(values.size(), conditionClause, statement);
        LOGGER.info("AbstractDAO-update-INFOR: " + statement.toString());
        try {
            return statement.executeUpdate();
        } finally {
            releaseResource(connection, statement);
        }
    }

    protected T queryGetOne(String query, ConditionClause conditionClause) throws Exception {
        final Connection connection = getConnection();
        if (conditionClause != null && !StringUtils.isEmpty(conditionClause.getConditions())) {
            query += conditionClause.getConditions();
        }
        final PreparedStatement statement = connection.prepareStatement(query);
        prepareConditionParameters(conditionClause, statement);
        LOGGER.info("AbstractDAO-queryGetOne-INFOR: " + statement.toString());
        final ResultSet resultSet = statement.executeQuery();
        try {
            int index = 1;
            while (resultSet.next()) {
                return mapFromResultSetToEntity(index++, resultSet);
            }
        } finally {
            releaseResource(connection, statement, resultSet);
        }
        return null;
    }

    protected DataSourceEnum getDataSourceEnum() {
        return this.dataSourceEnum;
    }

    protected Connection getConnection() throws Exception {
        final Connection connection = PoolSettingManager.getDataSource(getDataSourceEnum()).getConnection();
        if (connection == null) {
            LOGGER.error("AbstractDAO-getConnection-ERROR: connection is null");
            throw new SQLException("Exception occured when connection");
        }
        return connection;
    }

    protected String selectAllQuery() {
        return "Select * from " + getTableName();
    }

    protected void releaseResource(Connection connection, PreparedStatement statement) throws Exception {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                LOGGER.error("preStat.close(): " + ex.getMessage(), ex);
            }
        }
        
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            LOGGER.error("connection.close(): " + ex.getMessage(), ex);
        }
        
    }

    protected void releaseResource(Connection connection, List<PreparedStatement> statements) throws Exception {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            LOGGER.error("connection.close(): " + ex.getMessage(), ex);
        }
        
        if (statements != null) {
            statements.forEach((statement) -> {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    LOGGER.error("preStat.close(): " + ex.getMessage(), ex);
                }
            });
        }
    }

    protected void releaseResource(Connection connection, Statement statement, ResultSet resultSet) throws Exception {        
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            LOGGER.error("connection.close(): " + ex.getMessage(), ex);
        }
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (Exception ex) {
            LOGGER.error("statement.close(): " + ex.getMessage(), ex);
        }
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (Exception ex) {
            LOGGER.error("resultSet.close(): " + ex.getMessage(), ex);
        }
    }

    private void prepareConditionParameters(ConditionClause conditionClause, final PreparedStatement statement) throws Exception {
        if (conditionClause != null && !StringUtils.isEmpty(conditionClause.getConditions())) {
            for (Map.Entry<Integer, Object> entry : conditionClause.getValues().entrySet()) {
                final Object value = entry.getValue();
                final Integer key = entry.getKey();
                if (!setStatements(value, statement, key)) {
                    return;
                }
            }
        }
    }

    private void prepareConditionParameters(Map<Integer, Object> conditions, final PreparedStatement statement) throws Exception {
        if (conditions == null || conditions.isEmpty()) {
            return;
        }
        for (Map.Entry<Integer, Object> entry : conditions.entrySet()) {
            final Object value = entry.getValue();
            final Integer key = entry.getKey();
            if (!setStatements(value, statement, key)) {
                return;
            }
        }
    }

    private void prepareConditionParameters(int index, ConditionClause conditionClause, final PreparedStatement statement) throws Exception {
        if (conditionClause != null && !StringUtils.isEmpty(conditionClause.getConditions())) {
            for (Map.Entry<Integer, Object> entry : conditionClause.getValues().entrySet()) {
                final Object value = entry.getValue();
                final Integer key = entry.getKey();
                if (!setStatements(value, statement, index + key)) {
                    return;
                }
            }
        }
    }

    protected int executeQuery(String query, Map<Integer, Object> values) throws Exception {
        final Connection connection = getConnection();
        final PreparedStatement statement = connection.prepareStatement(query);
        prepareConditionParameters(values, statement);
        LOGGER.info(statement.toString());
        try {
            return statement.executeUpdate();
        } finally {
            releaseResource(connection, statement);
        }
    }
    
    
    protected int executeQuery(String query) throws Exception {
        final Connection connection = getConnection();
        final PreparedStatement statement = connection.prepareStatement(query);
        LOGGER.info(statement.toString());
        try {
            return statement.executeUpdate();
        } finally {
            releaseResource(connection, statement);
        }
    }

    protected void executeBatchQuery(BatchQuery batchQuery) throws Exception {
        final Connection connection = getConnection();
        connection.setAutoCommit(false);

        final Savepoint point = connection.setSavepoint();
        final List<PreparedStatement> statements = new ArrayList<>();
        try {
            for (QueryStatement query : batchQuery.queries) {
                final PreparedStatement statement = connection.prepareStatement(query.query);
                putStatementParameters(query.values, statement);
                LOGGER.info(statement.toString());
                statement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            connection.rollback(point);
            LOGGER.error("Exception", e);
        } finally {
            connection.setAutoCommit(true);
            releaseResource(connection, statements);
        }

    }

    public T findByKey(String query) throws Exception {
        final Connection connection = getConnection();
        final PreparedStatement statement = connection.prepareStatement(query);
        LOGGER.info(statement.toString());
        final ResultSet resultSet = statement.executeQuery();
        try {
            while (resultSet.next()) {
                return mapFromResultSetToEntity(1, resultSet);
            }
        } finally {
            releaseResource(connection, statement, resultSet);
        }
        return null;
    }

    private void putStatementParameters(LinkedHashMap<String, Object> values, final PreparedStatement statement) throws Exception {
        int index = 1;
        if (values != null && !values.isEmpty()) {
            for (Map.Entry<String, Object> entry : values.entrySet()) {
                setStatementParameter(index++, entry.getValue(), statement);
            }
        }
    }

    private void setStatementParameter(int index, Object value, final PreparedStatement statement) throws Exception {
        setStatements(value, statement, index);
    }

    private boolean setStatements(final Object value, final PreparedStatement statement, final Integer key) throws Exception {
        if (value instanceof String) {
            statement.setString(key, (String) value);
            return true;
        }
        if (value instanceof Integer) {
            statement.setInt(key, (Integer) value);
            return true;
        }
        if (value instanceof Long) {
            statement.setLong(key, (Long) value);
            return true;
        }
        if (value instanceof Date) {
            statement.setDate(key, (Date) value);
            return true;
        }
        if (value instanceof Timestamp) {
            statement.setTimestamp(key, (Timestamp) value);
            return true;
        }
        if (value instanceof BigDecimal) {
            statement.setBigDecimal(key, (BigDecimal) value);
            return true;
        }
        if (value instanceof Float) {
            statement.setFloat(key, (Float) value);
        }
        
        if (value instanceof Double) {
            statement.setDouble(key, (Double) value);
        }
        return false;
    }

//    protected abstract String setTableName();
    protected abstract T mapFromResultSetToEntity(int index, ResultSet rs) throws Exception;
}
