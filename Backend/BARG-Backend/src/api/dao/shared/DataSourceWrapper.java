/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao.shared;

import api.dao.pool.PoolSetting;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceWrapper {
        
    HikariDataSource ds = null;
    
    public DataSourceWrapper(PoolSetting setting){
        
        HikariConfig config = new HikariConfig();    
        config.setJdbcUrl(setting.getUrl());
        config.setUsername(setting.getUser());
        config.setPassword(setting.getPassword());
        config.setMaximumPoolSize(setting.getPoolSize());
        config.addDataSourceProperty("cachePrepStmts", ""+ setting.isCachePrepStmts());
        config.addDataSourceProperty("prepStmtCacheSize", "" + setting.getPrepStmtCacheSize());
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "" + setting.getPrepStmtCacheSqlLimit());      
        ds = new HikariDataSource(config);
    }
    
    public Connection getConnection() throws SQLException{
        
       return ds.getConnection();
    }
    
}
