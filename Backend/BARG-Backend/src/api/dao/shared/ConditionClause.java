/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao.shared;

import java.util.HashMap;
import java.util.Map;

public class ConditionClause {

    private final StringBuilder joins;
    private final StringBuilder conditions;
    private final StringBuilder groupBy;
    private final StringBuilder orderBy;
    private final StringBuilder limit;
    private final Map<Integer, Object> values;
    private boolean hasJoin = false;
    private boolean hasCondition = false;
    private boolean hasGroupBy = false;
    private boolean hasOrder = false;
    private boolean hasLimit = false;

    public ConditionClause() {
        joins = new StringBuilder();
        conditions = new StringBuilder("\nWHERE\n");
        groupBy = new StringBuilder("\nGROUP BY\n");
        orderBy = new StringBuilder("\nORDER BY\n");
        limit = new StringBuilder("\nLIMIT\n");
        values = new HashMap<>();
    }

    public ConditionClause startGroupCondition() {
        conditions.append("(");
        return this;
    }

    public ConditionClause endGroupCondition() {
        conditions.append(")");
        return this;
    }

    public ConditionClause pushLogicalCondition(boolean logic) {
        conditions.append(logic);
        hasCondition = true;
        return this;
    }

    public ConditionClause pushConditionEqual(String key, Object value) {
        conditions.append(key).append("=?");
        pushValue(value);
        hasCondition = true;
        return this;
    }
    
    public ConditionClause pushConditionSqlExpr(String sqlExpression) {
        conditions.append(sqlExpression);
        hasCondition = true;
        return this;
    }

    public ConditionClause in(String key, Object value) {
        conditions.append(key).append("\nin(?)");
        pushValue(value);
        hasCondition = true;
        return this;
    }

    public ConditionClause pushQuery(String query) {
        conditions.append(query);
        hasCondition = true;
        return this;
    }

    public ConditionClause pushJoin(String query) {
        joins.append(query);
        hasJoin = true;
        return this;
    }

    public ConditionClause lessThan(String key, Object value) {
        conditions.append(key).append("<?");
        pushValue(value);
        hasCondition = true;
        return this;
    }

    public ConditionClause lessThanAndEqual(String key, Object value) {
        conditions.append(key).append("<=?");
        pushValue(value);
        hasCondition = true;
        return this;
    }

    public ConditionClause greaterThan(String key, Object value) {
        conditions.append(key).append(">?");
        pushValue(value);
        hasCondition = true;
        return this;
    }

    public ConditionClause greaterThanAndEqual(String key, Object value) {
        conditions.append(key).append(">=?");
        pushValue(value);
        hasCondition = true;
        return this;
    }

    private void pushValue(Object value) {
        int index = values.size();
        values.put(++index, value);
    }

    public ConditionClause pushAnd() {
        conditions.append("\nAND\n");
        return this;
    }

    public ConditionClause pushOr() {
        conditions.append("\nOR\n");
        return this;
    }

    public ConditionClause limit(int start, int offset) {
        limit.append("\nlimit ?,?");
        pushValue(start);
        pushValue(offset);
        hasLimit = true;
        return this;
    }

    public ConditionClause orderBy(String key) {
        hasOrder = true;
        orderBy.append(key);
        return this;
    }

    public ConditionClause groupBy(String key) {
        groupBy.append(key);
        hasGroupBy = true;
        return this;
    }

    public String getConditions() {
        String condition = "";
        if (hasJoin) {
            condition += joins.toString();
        }
        if (hasCondition) {
            condition += conditions.toString();
        }
        if (hasGroupBy) {
            condition += groupBy.toString();
        }
        if (hasOrder) {
            condition += orderBy.toString();
        }
        if (hasLimit) {
            condition += limit.toString();
        }
        return condition;
    }

    public Map<Integer, Object> getValues() {
        return values;
    }
}
