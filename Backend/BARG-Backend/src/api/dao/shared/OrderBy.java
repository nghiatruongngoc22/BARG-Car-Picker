
package api.dao.shared;

public enum OrderBy {
    ASC(1, "ASC"),
    DESC(2, "DESC");
    private final int value;
    private final String displayValue;

    private OrderBy(int value, String displayValue) {
        this.value = value;
        this.displayValue = displayValue;
    }

    public int getValue() {
        return value;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public static OrderBy fromInt(int value) {
        for (OrderBy type : values()) {
            if (type.getValue() == value) {
                return type;
            }
        }
        return null;
    }
}
