/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao.shared;

import java.util.HashMap;

public enum DataSourceEnum {

    BARG_DB(1);

    private final int value;
    private static final HashMap<Integer, DataSourceEnum> returnMap = new HashMap<>();

    static {
        for (DataSourceEnum returnCodeEnum : DataSourceEnum.values()) {
            returnMap.put(returnCodeEnum.value, returnCodeEnum);
        }
    }

    DataSourceEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static DataSourceEnum fromInt(int iValue) {
        return returnMap.get(iValue);
    }

}
