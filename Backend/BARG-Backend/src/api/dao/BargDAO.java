/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao;

import api.dao.shared.AbstractDAO;
import api.dao.shared.DataSourceEnum;

/**
 *
 * @author NghiaTruongNgoc
 */
public abstract class BargDAO<T> extends AbstractDAO<T> {
    public BargDAO() {
        super();
    }
    
    @Override
    protected DataSourceEnum getDataSourceEnum() {
        return DataSourceEnum.BARG_DB;
    }
}
