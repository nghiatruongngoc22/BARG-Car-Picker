/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao;

import api.config.InitFileConfig;
import api.constant.TableNameConstants;
import api.dao.pool.PoolSettingManager;
import api.dao.shared.ConditionClause;
import api.entity.CarEntity;
import api.entity.CustomerInforEntity;
import api.entity.LatLng;
import api.enums.CustomerInforStatus;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTimeUtils;
import static server.main.Main.initBusiness;
import static utils.ApiUtils.reverseGeoCodingGoogle;
import utils.DateUtils;
import utils.LogUtils;

public class CustomerInforDAO extends BargDAO<CustomerInforEntity> {

    public CustomerInforDAO() {
        setTableName(TableNameConstants.CUSTOMER_TABLE);
    }

    @Override
    protected CustomerInforEntity mapFromResultSetToEntity(int index, ResultSet rs) throws Exception {
        final CustomerInforEntity entity = new CustomerInforEntity();

        entity.setId(rs.getInt("id"));
        entity.setCarID(rs.getInt("carID"));
        entity.setCustomerName(rs.getString("customerName"));
        entity.setLat(rs.getDouble("lat"));
        entity.setLng(rs.getDouble("lng"));
        entity.setPhoneNumber(rs.getString("phoneNumber"));
        entity.setStatus(rs.getInt("status"));
        entity.setAddress(rs.getString("address"));
        entity.setAdminActor(rs.getString("adminActor"));
        entity.setDescription(rs.getString("description"));
        entity.setRegDate(DateUtils.date2Str(new Date(rs.getLong("regDate"))));
        entity.setCarType(rs.getInt("carType"));
        
        entity.setRejectCount(rs.getInt("rejectCount"));

        return entity;
    }
    
    public CustomerInforEntity getInforByID(int id) {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(TableNameConstants.CUSTOMER_TABLE);
        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("id", id);
        try {
            return queryGetOne(statement.toString(), conditionClause);
        } catch (Exception ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public List<CustomerInforEntity> getInforByPhone(String phoneNumber) {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(TableNameConstants.CUSTOMER_TABLE);
        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("phoneNumber", phoneNumber);
        try {
            return queryGetMany(statement.toString(), conditionClause);
        } catch (Exception ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public List<CustomerInforEntity> getVerifiedInforByPhone(String phoneNumber) {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(TableNameConstants.CUSTOMER_TABLE);
        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("phoneNumber", phoneNumber);
        conditionClause.pushAnd();
        conditionClause.pushConditionEqual("status", 2);
        try {
            return queryGetMany(statement.toString(), conditionClause);
        } catch (Exception ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    
    
    public List<CustomerInforEntity> getExpiredCustomerInfo() {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(TableNameConstants.CUSTOMER_TABLE);
        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.lessThan("`regDate`", System.currentTimeMillis() - InitFileConfig.intervalTime);
        conditionClause.pushAnd();
        conditionClause.lessThanAndEqual("status", CustomerInforStatus.VERIFIED.getValue());
        try {
            return queryGetMany(statement.toString(), conditionClause);
        } catch (Exception ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    
    
    
    public List<CustomerInforEntity> getAll() {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(TableNameConstants.CUSTOMER_TABLE);
        try {
            return queryGetMany(statement.toString(), null);
        } catch (Exception ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public int insert(CustomerInforEntity entity) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();

        values.put("carID", entity.getCarID());
        values.put("customerName", entity.getCustomerName());
        values.put("lat", entity.getLat());
        values.put("lng", entity.getLng());
        values.put("phoneNumber", entity.getPhoneNumber());
        values.put("status", entity.getStatus());
        values.put("address", entity.getAddress());
        values.put("adminActor", entity.getAdminActor());
        values.put("description", entity.getDescription());
        values.put("regDate", System.currentTimeMillis());
        values.put("carType", entity.getCarType());

        return insert(values);
    }

    public int markStatus(int id, int status, String username) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();
        
        values.put("status", status);
        values.put("adminActor", username);

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("id", id);

        return update(values, conditionClause);
    }
    
    
    public int updateActor(int id, String username) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();
        values.put("adminActor", username);

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("id", id);

        return update(values, conditionClause);
    }
    
    public int updateLocation(int id, double lat, double lng, String username, String address) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();
        
        values.put("lat", lat);
        values.put("lng", lng);
        values.put("address", address);
        values.put("adminActor", username);

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("id", id);

        return update(values, conditionClause);
    }
    
    public int updateInfo(int id, int carID) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();
        
        values.put("carID", carID);

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("id", id);

        return update(values, conditionClause);
    }
    
    
    public int updateCount(int id, int count) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();
        
        values.put("rejectCount", count);

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("id", id);

        return update(values, conditionClause);
    }
    
    
}
