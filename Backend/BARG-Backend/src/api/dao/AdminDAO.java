/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.dao;

import api.constant.TableNameConstants;
import api.dao.pool.PoolSettingManager;
import api.dao.shared.ConditionClause;
import api.entity.AdminEntity;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static server.main.Main.initBusiness;
import utils.HashUtils;

public class AdminDAO extends BargDAO<AdminEntity> {

    public AdminDAO() {
        setTableName(TableNameConstants.ADMIN_TABLE);
    }

    private String password = "";

    @Override
    protected AdminEntity mapFromResultSetToEntity(int index, ResultSet rs) throws Exception {
        final AdminEntity entity = new AdminEntity();

        entity.setUsername(rs.getString("username"));
        entity.setDescription(rs.getString("description"));
        entity.setRole(rs.getInt("role"));
        entity.setAdminsig(rs.getString("adminSig"));
        password = rs.getString("password");

        return entity;
    }   

    public List<AdminEntity> getAllAdmin() throws Exception {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(TableNameConstants.ADMIN_TABLE);
        
        return queryGetMany(statement.toString(), null);
    }

    public int update(AdminEntity entity) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();

        values.put("adminSig", entity.getAdminsig());
        values.put("password", password);

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("username", entity.getUsername());

        return update(values, conditionClause);
    }

    public AdminEntity getAminByUsername(String username, int role) {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(TableNameConstants.ADMIN_TABLE);
        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("username", username);
        conditionClause.pushAnd();
        conditionClause.pushConditionEqual("`role`", role);
        try {
            return queryGetOne(statement.toString(), conditionClause);
        } catch (Exception ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public AdminEntity getAminByUsername(String username) {
        final StringBuilder statement = new StringBuilder("SELECT * FROM").append(" ").append(TableNameConstants.ADMIN_TABLE);
        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("username", username);
        try {
            return queryGetOne(statement.toString(), conditionClause);
        } catch (Exception ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public String updateAdminSig(String username) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();
        
        String adminSig = buildAdminSig(username);
        
        values.put("adminSig", adminSig);

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("username", username);

        update(values, conditionClause);
        
        return adminSig;
    }
    
    public boolean logout(String username, String sig) throws Exception {
        final LinkedHashMap<String, Object> values = new LinkedHashMap<>();
        
        if (!checkAdminSig(username, sig)) {
            return false;
        }
        
        values.put("adminSig", "");

        final ConditionClause conditionClause = new ConditionClause();
        conditionClause.pushConditionEqual("username", username);

        update(values, conditionClause);
        
        return true;
    }
    
    
    public boolean checkAdminSig(String username, String sig) {
        AdminEntity entity = getAminByUsername(username);
        
        if (entity != null && entity.getAdminsig().equals(sig)) {
            return true;
        }
        
        return false;
    }
    
    
    public AdminEntity login(String username, String password, int role) throws Exception {
        AdminEntity entity = getAminByUsername(username, role);
        
        if (validatePassword(this.password, password) && entity != null) {
            String adminSig = updateAdminSig(username);
            entity.setAdminsig(adminSig);
            return entity;
        } else {
            return null;
        }
    }
    

    private String buildAdminSig(String username) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append(username).append("|")
                .append(password).append("|")
                .append(System.currentTimeMillis());

        try {
            return HashUtils.hashSHA256(stringBuilder.toString());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AdminDAO.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    private boolean validatePassword(String password, String passwordReq) {
        return password.equals(passwordReq);
    }
    
    
    public static void main(String args[]) throws Exception {
        initBusiness();
        
        if (!PoolSettingManager.loadBargDB()) {
            throw new Exception("load setting db config fail");
        }
        
        List<AdminEntity> admins = new AdminDAO().getAllAdmin();
        
        for (AdminEntity entity : admins) {
            System.out.println(entity.getUsername());
        }
    }

}
