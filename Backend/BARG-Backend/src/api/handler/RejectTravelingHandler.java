/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.config.InitFileConfig;
import api.dao.CarDAO;
import api.dao.CustomerInforDAO;
import api.entity.BaseRequestInfo;
import api.entity.CarEntity;
import api.entity.CustomerInforEntity;
import api.entity.GetCarLocationResponse;
import api.enums.APIStatusEnum;
import api.enums.CustomerInforStatus;
import api.factory.BaseHandler;
import org.apache.log4j.Logger;
import utils.FirebaseUtils;
import utils.GsonUtils;

public class RejectTravelingHandler extends BaseHandler {

    private static final Logger logger = Logger.getLogger("UpdateCarLocation");
    private static final RejectTravelingHandler instance = new RejectTravelingHandler();

    private static GetCarLocationResponse response = new GetCarLocationResponse();

    public RejectTravelingHandler() {

    }

    public static RejectTravelingHandler getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new GetCarLocationResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {

                String carSig = requestInfo.getParam("carsig");

                String username = requestInfo.getParam("drivername");

                int id = Integer.parseInt(requestInfo.getParam("id"));

                if (!new CarDAO().checkCarSig(username, carSig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_CAR_SIG);
                } else {
                    try {
                        rejectTraveling(id);
                        setResponseStatus(response, APIStatusEnum.SUCCESS);
                    } catch (Exception ex) {
                        logger.error("BeginTravelingHandler-handle-ERROR: " + ex.getMessage(), ex);
                        setResponseStatus(response, APIStatusEnum.EXCEPTION);
                    }
                }

            }

        } catch (Exception ex) {
            logger.error("BeginTravelingHandler-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private void rejectTraveling(int customerID) throws Exception {
        CustomerInforDAO customerDao = new CustomerInforDAO();
        CustomerInforEntity entity = customerDao.getInforByID(customerID);

        if (entity.getStatus() != CustomerInforStatus.VERIFIED.getValue()) {
            setResponseStatus(response, APIStatusEnum.ANOTHER_DRIVER_TOOK);
            return;
        }

        if (entity.getRejectCount() + 1 >= InitFileConfig.rejectCountCfg) {
            
            entity.setStatus(CustomerInforStatus.CAR_NOT_FOUND.getValue());
            entity.setAdminActor("Server");
            customerDao.markStatus(customerID, CustomerInforStatus.CAR_NOT_FOUND.getValue(), "Server");
            setResponseStatus(response, APIStatusEnum.NO_DRIVER);
            FirebaseUtils.updateCustomerInfo(entity);
            
            return;
        }

        entity.setRejectCount(entity.getRejectCount() + 1);
        customerDao.updateCount(customerID, entity.getRejectCount());

        FirebaseUtils.updateCustomerInfo(entity);
    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

}
