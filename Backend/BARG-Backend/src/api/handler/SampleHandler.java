/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.entity.BaseRequestInfo;
import api.factory.BaseHandler;
import org.apache.log4j.Logger;
import utils.GsonUtils;

public class SampleHandler extends BaseHandler {

    private static final Logger logger = Logger.getLogger("SampleHandler");
    private static final SampleHandler instance = new SampleHandler();

    public SampleHandler() {

    }

    public static SampleHandler getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {
            String uid = requestInfo.getParam("uid");

            int a = 0;

        } catch (Exception ex) {
            logger.error("SampleHandler-handle-ERROR: " + ex.getMessage(), ex);
        }

        return GsonUtils.toJsonString("Sample API Response");
    }
}
