/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.dao.CarDAO;
import api.entity.BaseRequestInfo;
import api.entity.BaseResponse;
import api.entity.LoginResponse;
import api.enums.APIStatusEnum;
import api.factory.BaseHandler;
import org.apache.log4j.Logger;
import utils.GsonUtils;

public class CarLogoutHandler extends BaseHandler {

    private static final Logger logger = Logger.getLogger("LogoutHandler");
    private static final CarLogoutHandler instance = new CarLogoutHandler();
    
    private static BaseResponse response = new BaseResponse();

    public CarLogoutHandler() {

    }

    public static CarLogoutHandler getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new LoginResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                String username = requestInfo.getParam("drivername");
                String sig = requestInfo.getParam("sig");

                checkLogout(username, sig);
            }

        } catch (Exception ex) {
            logger.error("LogoutHandler-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);
    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

    private void checkLogout(String username, String sig) throws Exception {
        boolean canLogout = new CarDAO().logout(username, sig);
        
        if (canLogout == false) {
            setResponseStatus(response, APIStatusEnum.INVALID_CAR_SIG);
            return;
        }
        
        setResponseStatus(response, APIStatusEnum.SUCCESS);
    }

}
