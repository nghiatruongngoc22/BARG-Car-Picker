/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.dao.CarDAO;
import api.dao.CustomerInforDAO;
import api.entity.AdminEntity;
import api.entity.BaseRequestInfo;
import api.entity.BaseResponse;
import api.entity.CarEntity;
import api.entity.CustomerInforEntity;
import api.entity.GetCustomerInforResponse;
import api.entity.GetVerifiedCustomerInforResponse;
import api.entity.LoginResponse;
import api.enums.APIStatusEnum;
import api.enums.CustomerInforStatus;
import api.factory.BaseHandler;
import com.google.gson.JsonObject;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import static utils.ApiUtils.geoCodingGoogle;
import utils.GsonUtils;

public class GetVerifiedCustomerInfor extends BaseHandler {

    private static final Logger logger = Logger.getLogger("GetVerifiedCustomerInfor");
    private static final GetVerifiedCustomerInfor instance = new GetVerifiedCustomerInfor();

    private static GetVerifiedCustomerInforResponse response = new GetVerifiedCustomerInforResponse();

    public GetVerifiedCustomerInfor() {

    }

    public static GetVerifiedCustomerInfor getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new GetVerifiedCustomerInforResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                int id = Integer.parseInt(requestInfo.getParam("id"));

                String adminsig = requestInfo.getParam("adminsig");
                String username = requestInfo.getParam("username");

                if (!new AdminDAO().checkAdminSig(username, adminsig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_ADMIN_SIG);
                } else {
                    if (!checkValidStatus(id)) {
                        setResponseStatus(response, APIStatusEnum.NOT_HANDLED_INFOR);
                    } else {
                        CustomerInforEntity entity = getCustomerInfo(id);
                        CarEntity carEntity = new CarDAO().getCarByID(entity.getCarID());
                        
                        response.setCustomerinfo(entity);
                        response.setCar_info(carEntity);
                        
                        setResponseStatus(response, APIStatusEnum.SUCCESS);
                    }

                }
            }

        } catch (Exception ex) {
            logger.error("AddCustomerInfor-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

    private boolean checkValidStatus(int id) {
        CustomerInforEntity entity = new CustomerInforDAO().getInforByID(id);

        if (entity.getStatus() != CustomerInforStatus.END_TRAVEL.getValue()) {
            return false;
        }

        return true;
    }

    private CustomerInforEntity getCustomerInfo(int id) throws Exception {
        CustomerInforDAO dao = new CustomerInforDAO();
        CustomerInforEntity entity = dao.getInforByID(id);
         
        return entity;
       
    }

}
