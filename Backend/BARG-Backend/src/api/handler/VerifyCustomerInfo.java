/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.dao.CarDAO;
import api.dao.CustomerInforDAO;
import api.dao.pool.PoolSettingManager;
import api.entity.BaseRequestInfo;
import api.entity.BaseResponse;
import api.entity.CarEntity;
import api.entity.CarList;
import api.entity.CustomerInforEntity;
import api.enums.APIStatusEnum;
import api.enums.CustomerInforStatus;
import api.factory.BaseHandler;
import org.apache.log4j.Logger;
import static server.main.Main.initBusiness;
import utils.FirebaseUtils;
import utils.GsonUtils;
import utils.LogUtils;

public class VerifyCustomerInfo extends BaseHandler {

    private static final Logger logger = Logger.getLogger("VerifyCustomerInfo");
    private static final VerifyCustomerInfo instance = new VerifyCustomerInfo();

    private static BaseResponse response = new BaseResponse();

    public VerifyCustomerInfo() {

    }

    public static VerifyCustomerInfo getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new BaseResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                int id = Integer.parseInt(requestInfo.getParam("id"));
                //String carlistStr = requestInfo.getParam("carlist");
                
                        
                String adminsig = requestInfo.getParam("adminsig");
                String username = requestInfo.getParam("username");

                if (!new AdminDAO().checkAdminSig(username, adminsig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_ADMIN_SIG);
                } else {

                    markStatus(id, username);
                    
                    //chooseCar(carlistStr, id);

                    //push notification
                    setResponseStatus(response, APIStatusEnum.SUCCESS);
                }
            }

        } catch (Exception ex) {
            logger.error("MarkInforStatus-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

    private void markStatus(int id, String username) throws Exception {

        CustomerInforDAO dao = new CustomerInforDAO();
        dao.markStatus(id, CustomerInforStatus.VERIFIED.getValue(), username);
        
        CustomerInforEntity entitty = dao.getInforByID(id);
        FirebaseUtils.updateCustomerInfo(entitty);
    }
    
    public static void main(String args[]) throws Exception {
        
        LogUtils.init();

        initBusiness();
        
        if (!PoolSettingManager.loadBargDB()) {
            throw new Exception("load setting db config fail");
        }
        String car = "{\"carlist\":[{\"id\":1,\"description\":\"Car number 1\",\"pickAddress\":\"\",\"type\":2,\"isBusy\":0,\"lng\":106.68164250283006,\"lat\":10.762265796622136,\"distance\":145},{\"id\":2,\"description\":\"Car number 1\",\"pickAddress\":\"\",\"type\":2,\"isBusy\":0,\"lng\":106.68164250283006,\"lat\":10.762265796622136,\"distance\":145}]}";
        
        CarList list = GsonUtils.fromJsonString(car, CarList.class);
        
        int a = 0;
    }

    private void chooseCar(String carStr, int id) throws Exception {
        CustomerInforDAO dao = new CustomerInforDAO();
        CustomerInforEntity entity = dao.getInforByID(id);
        
        CarList list = GsonUtils.fromJsonString(carStr, CarList.class);
        
        if (list.getCarlist().isEmpty()) {
            return;
        }
        
        // chon xe dau tien
        
        CarEntity car = list.getCarlist().get(0);
        
        car.setIsBusy(1);
        car.setPickAddress(entity.getAddress());
        
        new CarDAO().updateCar(car.getId(), car.getIsBusy(), car.getPickAddress());
        entity.setCarID(car.getId());
        dao.updateInfo(id, car.getId());
        
        FirebaseUtils.updateCustomerInfo(entity);
    }

}
