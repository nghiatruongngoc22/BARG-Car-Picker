/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.dao.CarDAO;
import api.dao.CustomerInforDAO;
import api.entity.AdminEntity;
import api.entity.BaseRequestInfo;
import api.entity.BaseResponse;
import api.entity.CarEntity;
import api.entity.CustomerInforEntity;
import api.entity.LoginResponse;
import api.enums.APIStatusEnum;
import api.factory.BaseHandler;
import com.google.gson.JsonObject;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import static utils.ApiUtils.geoCodingGoogle;
import utils.FirebaseUtils;
import utils.GsonUtils;

public class AddCustomerInfor extends BaseHandler {

    private static final Logger logger = Logger.getLogger("AddCustomerInfor");
    private static final AddCustomerInfor instance = new AddCustomerInfor();

    private static BaseResponse response = new BaseResponse();

    public AddCustomerInfor() {

    }

    public static AddCustomerInfor getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new BaseResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                String customerName = requestInfo.getParam("customername");
                String phoneNumber = requestInfo.getParam("phonenumber");
                String address = requestInfo.getParam("address");
                String adminsig = requestInfo.getParam("adminsig");
                String username = requestInfo.getParam("username");
                String descr = requestInfo.getParam("descr");
                int carType = Integer.parseInt(requestInfo.getParam("cartype"));

                if (!new AdminDAO().checkAdminSig(username, adminsig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_ADMIN_SIG);
                } else {

                    int id = insertCustomerInfor(customerName, phoneNumber, address, descr, carType);

                    CustomerInforEntity entity = new CustomerInforDAO().getInforByID(id);

                    if (!handleInforSuccessInPast(entity)) //push notification
                    {
                        FirebaseUtils.updateCustomerInfo(entity);
                        setResponseStatus(response, APIStatusEnum.SUCCESS);
                    }

                }
            }

        } catch (Exception ex) {
            logger.error("AddCustomerInfor-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

    private int insertCustomerInfor(String customerName, String phoneNumber, String address, String descr, int carType) throws Exception {

        CustomerInforDAO dao = new CustomerInforDAO();
        CustomerInforEntity entity = new CustomerInforEntity();

        entity.setCustomerName(customerName);
        entity.setPhoneNumber(phoneNumber);
        entity.setAddress(address);
        entity.setDescription(descr);
        entity.setCarType(carType);

        return dao.insert(entity);
    }

    private boolean handleInforSuccessInPast(CustomerInforEntity entityReal) throws Exception {
        CustomerInforDAO dao = new CustomerInforDAO();
        List<CustomerInforEntity> entities = dao.getVerifiedInforByPhone(entityReal.getPhoneNumber());

        for (CustomerInforEntity entity : entities) {
            if (entity.getAddress().toLowerCase().trim().equals(entityReal.getAddress().toLowerCase().trim())) {
                entityReal.setStatus(1);

                dao.markStatus(entityReal.getId(), entityReal.getStatus(), "ServerHandle");
                FirebaseUtils.updateCustomerInfo(entityReal);

                entityReal.setAddress(entity.getAddress());
                entityReal.setLat(entity.getLat());
                entityReal.setLng(entity.getLng());

                dao.updateLocation(entityReal.getId(), entityReal.getLat(),
                        entityReal.getLng(), "ServerHandle", entityReal.getAddress());

                List<CarEntity> cars = new CarDAO().getAvailableCarByRadius(
                        entityReal.getCarType(),
                        1000, entityReal.getLat(), entityReal.getLng());

                if (cars.isEmpty()) {
                    entityReal.setStatus(2);
                    entityReal.setCarID(-1);
                    setResponseStatus(response, APIStatusEnum.EMPTY_CAR_SERVER_HANDLING);
                    dao.markStatus(entityReal.getId(), entityReal.getStatus(), "ServerHandle");
                    FirebaseUtils.updateCustomerInfo(entityReal);
                    return true;
                }

                CarEntity carToChoose = cars.get(0);
                carToChoose.setPickAddress(entityReal.getAddress());
                carToChoose.setIsBusy(1);

                new CarDAO().updateCar(carToChoose.getId(), carToChoose.getIsBusy(), carToChoose.getPickAddress());

                entityReal.setStatus(2);
                entityReal.setCarID(carToChoose.getId());
                FirebaseUtils.updateCustomerInfo(entityReal);
                dao.updateInfo(entityReal.getId(), entityReal.getCarID());
                dao.markStatus(entityReal.getId(), entityReal.getStatus(), "ServerHandle");

                setResponseStatus(response, APIStatusEnum.SERVER_HANDLING);

                return true;
            }
        }

        return false;
    }

}
