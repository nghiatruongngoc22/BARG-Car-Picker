/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.CarDAO;
import api.dao.CustomerInforDAO;
import api.entity.BaseRequestInfo;
import api.entity.CarEntity;
import api.entity.CustomerInforEntity;
import api.entity.GetCarLocationResponse;
import api.enums.APIStatusEnum;
import api.enums.CustomerInforStatus;
import api.factory.BaseHandler;
import org.apache.log4j.Logger;
import utils.FirebaseUtils;
import utils.GsonUtils;

public class EndTravelingHandler extends BaseHandler {

    private static final Logger logger = Logger.getLogger("EndTravelingHandler");
    private static final EndTravelingHandler instance = new EndTravelingHandler();

    private static GetCarLocationResponse response = new GetCarLocationResponse();

    public EndTravelingHandler() {

    }

    public static EndTravelingHandler getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new GetCarLocationResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                int carID = Integer.parseInt(requestInfo.getParam("carid"));

                String carSig = requestInfo.getParam("carsig");

                String username = requestInfo.getParam("drivername");

                int id = Integer.parseInt(requestInfo.getParam("id"));

                if (!new CarDAO().checkCarSig(username, carSig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_CAR_SIG);
                } else {
                    try {
                        endTraveling(carID, id, username);
                        setResponseStatus(response, APIStatusEnum.SUCCESS);
                    } catch (Exception ex) {
                        logger.error("EndTravelingHandler-handle-ERROR: " + ex.getMessage(), ex);
                        setResponseStatus(response, APIStatusEnum.EXCEPTION);
                    }
                }
            }

        } catch (Exception ex) {
            logger.error("EndTravelingHandler-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private void endTraveling(int carID, int customerID, String drivername) throws Exception {
        CarDAO dao = new CarDAO();
        CustomerInforDAO customerDao = new CustomerInforDAO();

        CarEntity car = dao.getCarByID(carID);
        CustomerInforEntity entity = customerDao.getInforByID(customerID);
        

        car.setIsBusy(0);
        car.setPickAddress("");

        new CarDAO().updateCar(car.getId(), car.getIsBusy(), car.getPickAddress());
        entity.setCarID(car.getId());
        customerDao.markStatus(customerID, CustomerInforStatus.END_TRAVEL.getValue(), drivername);

        
        entity.setStatus(CustomerInforStatus.END_TRAVEL.getValue());
        entity.setAdminActor(drivername);
        customerDao.updateInfo(customerID, car.getId());
 
        FirebaseUtils.updateCustomerInfo(entity);
    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

}
