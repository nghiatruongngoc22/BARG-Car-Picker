/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.dao.CustomerInforDAO;
import api.entity.BaseRequestInfo;
import api.entity.CustomerInforEntity;
import api.entity.GeoCodingResponse;
import api.entity.GetCustomerInforResponse;
import api.enums.APIStatusEnum;
import api.enums.CustomerInforStatus;
import api.factory.BaseHandler;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import utils.ApiUtils;
import utils.FirebaseUtils;
import utils.GsonUtils;

public class ReverseGeoCodingHandler extends BaseHandler {

    private static final Logger logger = Logger.getLogger("GetCustomerInfor");
    private static final ReverseGeoCodingHandler instance = new ReverseGeoCodingHandler();

    private static GeoCodingResponse response = new GeoCodingResponse();

    public ReverseGeoCodingHandler() {

    }

    public static ReverseGeoCodingHandler getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new GeoCodingResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                double lat = Double.parseDouble(requestInfo.getParam("lat"));
                double lng = Double.parseDouble(requestInfo.getParam("lng"));
                int id = Integer.parseInt(requestInfo.getParam("id"));

                String adminsig = requestInfo.getParam("adminsig");
                String username = requestInfo.getParam("username");

                if (!new AdminDAO().checkAdminSig(username, adminsig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_ADMIN_SIG);
                } else {
                    String resultMsgGoogle = ApiUtils.reverseGeoCodingGoogle(lat, lng);

                    handleGoogleResponse(resultMsgGoogle, id, username);
                }
            }

        } catch (Exception ex) {
            logger.error("AddCustomerInfor-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }


    private void handleGoogleResponse(String resultMsgGoogle, int id, String username) throws Exception {
        JSONObject jObj = new JSONObject(resultMsgGoogle);

        JSONArray result = jObj.getJSONArray("results");
        String address = result.getJSONObject(0).getString("formatted_address");
        double lat = result.getJSONObject(0).getJSONObject("geometry")
                .getJSONObject("location").getDouble("lat");
        double lng = result.getJSONObject(0).getJSONObject("geometry")
                .getJSONObject("location").getDouble("lng");
        
        CustomerInforDAO dao = new CustomerInforDAO();
        
        dao.updateLocation(id, lat, lng, username, address);
        
        FirebaseUtils.updateCustomerInfo(dao.getInforByID(id));

        response.setLat(lat);
        response.setLng(lng);
        response.setFormatted_address(address);
        setResponseStatus(response, APIStatusEnum.SUCCESS);
    }

}
