/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.dao.CarDAO;
import api.dao.CustomerInforDAO;
import api.dao.pool.PoolSettingManager;
import api.entity.AdminEntity;
import api.entity.BaseRequestInfo;
import api.entity.BaseResponse;
import api.entity.CarEntity;
import api.entity.CarListResponse;
import api.entity.CustomerInforEntity;
import api.entity.LoginResponse;
import api.enums.APIStatusEnum;
import api.factory.BaseHandler;
import com.google.gson.JsonObject;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import static server.main.Main.initBusiness;
import static utils.ApiUtils.geoCodingGoogle;
import utils.GsonUtils;
import utils.LogUtils;

public class GetAvailableCarListHandler extends BaseHandler {

    private static final Logger logger = Logger.getLogger("GetAvailableCarListHandler");
    private static final GetAvailableCarListHandler instance = new GetAvailableCarListHandler();

    private static CarListResponse response = new CarListResponse();

    public GetAvailableCarListHandler() {

    }

    public static GetAvailableCarListHandler getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new CarListResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                int id = Integer.parseInt(requestInfo.getParam("id"));
                
                        
                String adminsig = requestInfo.getParam("adminsig");
                String username = requestInfo.getParam("username");

                if (!new AdminDAO().checkAdminSig(username, adminsig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_ADMIN_SIG);
                } else {
                    CustomerInforEntity entity = new CustomerInforDAO().getInforByID(id);
                    
                    double lat = entity.getLat();
                    double lng = entity.getLng();
                    
                    List<CarEntity> carList = new CarDAO().getAvailableCarByRadius(entity.getCarType(), 300, lat, lng);
                    
                    if (carList.isEmpty()) {
                        carList = new CarDAO().getAvailableCarByRadius(entity.getCarType(), 600, lat, lng);
                    }
                    
                    if (carList.isEmpty()) {
                        carList = new CarDAO().getAvailableCarByRadius(entity.getCarType(), 1000, lat, lng);
                    }

                    response.setCarlist(carList);
                    setResponseStatus(response, APIStatusEnum.SUCCESS);
                }
            }

        } catch (Exception ex) {
            logger.error("MarkInforStatus-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }
    
    public static void main(String args[]) throws Exception {
        
        LogUtils.init();

        initBusiness();
        
        if (!PoolSettingManager.loadBargDB()) {
            throw new Exception("load setting db config fail");
        }
        CustomerInforEntity entity = new CustomerInforDAO().getInforByID(1);
        
        System.out.println(GsonUtils.toJsonString(entity));
    }

}
