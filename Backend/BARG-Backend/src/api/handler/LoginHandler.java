/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.entity.AdminEntity;
import api.entity.BaseRequestInfo;
import api.entity.LoginResponse;
import api.enums.APIStatusEnum;
import api.factory.BaseHandler;
import com.google.gson.JsonObject;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import static utils.ApiUtils.geoCodingGoogle;
import utils.GsonUtils;

public class LoginHandler extends BaseHandler {

    private static final Logger logger = Logger.getLogger("LoginHandler");
    private static final LoginHandler instance = new LoginHandler();

    private static LoginResponse response = new LoginResponse();

    public LoginHandler() {

    }

    public static LoginHandler getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new LoginResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                String username = requestInfo.getParam("username");
                String password = requestInfo.getParam("password");
                int roleStr = Integer.parseInt(requestInfo.getParam("role"));

                checkLogin(username, password, roleStr);
            }

        } catch (Exception ex) {
            logger.error("LoginHandler-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

    private void checkLogin(String username, String password, int role) throws Exception {
        AdminEntity entity = new AdminDAO().login(username, password, role);

        if (entity == null) {
            setResponseStatus(response, APIStatusEnum.ADMIN_NOT_FOUND);
            return;
        }

        setResponseStatus(response, APIStatusEnum.SUCCESS);
        response.setAdmininfo(entity);
    }

}
