/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.dao.CustomerInforDAO;
import api.entity.BaseRequestInfo;
import api.entity.CustomerInforEntity;
import api.entity.GetCustomerInforResponse;
import api.enums.APIStatusEnum;
import api.enums.CustomerInforStatus;
import api.factory.BaseHandler;
import org.apache.log4j.Logger;
import utils.FirebaseUtils;
import utils.GsonUtils;

public class GetCustomerInfor extends BaseHandler {

    private static final Logger logger = Logger.getLogger("GetCustomerInfor");
    private static final GetCustomerInfor instance = new GetCustomerInfor();

    private static GetCustomerInforResponse response = new GetCustomerInforResponse();

    public GetCustomerInfor() {

    }

    public static GetCustomerInfor getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new GetCustomerInforResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                int id = Integer.parseInt(requestInfo.getParam("id"));

                String adminsig = requestInfo.getParam("adminsig");
                String username = requestInfo.getParam("username");

                if (!new AdminDAO().checkAdminSig(username, adminsig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_ADMIN_SIG);
                } else {
                    if (!checkValidStatus(id, username)) {
                        setResponseStatus(response, APIStatusEnum.HANDLED_INFOR);
                    } else {
                        CustomerInforEntity entity = updateCustomerInfo(id, username);
                        
                        response.setCustomerinfo(entity);
                        
                        setResponseStatus(response, APIStatusEnum.SUCCESS);
                    }

                }
            }

        } catch (Exception ex) {
            logger.error("AddCustomerInfor-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

    private boolean checkValidStatus(int id, String username) {
        CustomerInforEntity entity = new CustomerInforDAO().getInforByID(id);

        if ((entity.getStatus() == CustomerInforStatus.PROCESSING.getValue() 
                && !entity.getAdminActor().equals(username))
                || entity.getStatus() >= CustomerInforStatus.VERIFIED.getValue()) {
            return false;
        }

        return true;
    }

    private CustomerInforEntity updateCustomerInfo(int id, String username) throws Exception {
        CustomerInforDAO dao = new CustomerInforDAO();
        dao.markStatus(id, CustomerInforStatus.PROCESSING.getValue(), username);
        CustomerInforEntity entity = dao.getInforByID(id);
         //FIREBASE
         
        FirebaseUtils.updateCustomerInfo(entity);
         
        return entity;
       
    }

}
