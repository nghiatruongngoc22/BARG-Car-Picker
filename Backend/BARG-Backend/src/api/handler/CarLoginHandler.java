/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.dao.CarDAO;
import api.entity.AdminEntity;
import api.entity.BaseRequestInfo;
import api.entity.CarEntity;
import api.entity.CarLoginResponse;
import api.entity.LoginResponse;
import api.enums.APIStatusEnum;
import api.factory.BaseHandler;
import org.apache.log4j.Logger;
import utils.GsonUtils;

public class CarLoginHandler extends BaseHandler {

    private static final Logger logger = Logger.getLogger("CarLoginHandler");
    private static final CarLoginHandler instance = new CarLoginHandler();

    private static CarLoginResponse response = new CarLoginResponse();

    public CarLoginHandler() {

    }

    public static CarLoginHandler getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new CarLoginResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                String username = requestInfo.getParam("drivername");
                String password = requestInfo.getParam("password");

                checkLogin(username, password);
            }

        } catch (Exception ex) {
            logger.error("CarLoginHandler-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

    private void checkLogin(String username, String password) throws Exception {
        CarEntity entity = new CarDAO().login(username, password);

        if (entity == null) {
            setResponseStatus(response, APIStatusEnum.DRIVER_NOT_FOUND);
            return;
        }

        setResponseStatus(response, APIStatusEnum.SUCCESS);
        response.setCarinfo(entity);
    }

}
