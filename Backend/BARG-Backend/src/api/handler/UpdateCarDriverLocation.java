/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.CarDAO;
import api.entity.BaseRequestInfo;
import api.entity.GetCarLocationResponse;
import api.enums.APIStatusEnum;
import api.factory.BaseHandler;
import org.apache.log4j.Logger;
import utils.GsonUtils;

public class UpdateCarDriverLocation extends BaseHandler {

    private static final Logger logger = Logger.getLogger("GetCarLocation");
    private static final UpdateCarDriverLocation instance = new UpdateCarDriverLocation();

    private static GetCarLocationResponse response = new GetCarLocationResponse();

    public UpdateCarDriverLocation() {

    }

    public static UpdateCarDriverLocation getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new GetCarLocationResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                int carID = Integer.parseInt(requestInfo.getParam("carID"));

                String carSig = requestInfo.getParam("carsig");

                String username = requestInfo.getParam("driverName");
                
                double lat = Double.parseDouble(requestInfo.getParam("lat"));
                double lng = Double.parseDouble(requestInfo.getParam("lng"));
                
                //validate carsig

//                if (!new AdminDAO().checkAdminSig(username, adminsig)) {
//                    setResponseStatus(response, APIStatusEnum.INVALID_ADMIN_SIG);
//                } else {
//
//                    markStatus(id, status, username);
//
//                    //push notification
//                    setResponseStatus(response, APIStatusEnum.SUCCESS);
//                }
                if (new CarDAO().updateCarLocation(carID, lat, lng)) {
                    setResponseStatus(response, APIStatusEnum.SUCCESS);
                } else {
                    setResponseStatus(response, APIStatusEnum.CAR_NOT_FOUND);
                }
            }

        } catch (Exception ex) {
            logger.error("MarkInforStatus-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

}
