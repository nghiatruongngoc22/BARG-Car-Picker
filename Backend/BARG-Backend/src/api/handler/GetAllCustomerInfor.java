/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.dao.CustomerInforDAO;
import api.entity.AdminEntity;
import api.entity.BaseRequestInfo;
import api.entity.BaseResponse;
import api.entity.CustomerInforEntity;
import api.entity.GetAllCustomerInfoResponse;
import api.entity.GetCustomerInforResponse;
import api.entity.LoginResponse;
import api.enums.APIStatusEnum;
import api.enums.CustomerInforStatus;
import api.factory.BaseHandler;
import com.google.gson.JsonObject;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import static utils.ApiUtils.geoCodingGoogle;
import utils.GsonUtils;

public class GetAllCustomerInfor extends BaseHandler {

    private static final Logger logger = Logger.getLogger("GetAllCustomerInfor");
    private static final GetAllCustomerInfor instance = new GetAllCustomerInfor();

    private static GetAllCustomerInfoResponse response = new GetAllCustomerInfoResponse();

    public GetAllCustomerInfor() {

    }

    public static GetAllCustomerInfor getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new GetAllCustomerInfoResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                String adminsig = requestInfo.getParam("adminsig");
                String username = requestInfo.getParam("username");

                if (!new AdminDAO().checkAdminSig(username, adminsig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_ADMIN_SIG);
                } else {
                    List<CustomerInforEntity> entities = new CustomerInforDAO().getAll();

                    response.setCustomerinfo_list(entities);
                    setResponseStatus(response, APIStatusEnum.SUCCESS);
                }
            }

        } catch (Exception ex) {
            logger.error("AddCustomerInfor-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

}
