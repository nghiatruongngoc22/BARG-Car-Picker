/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.dao.AdminDAO;
import api.dao.CarDAO;
import api.dao.CustomerInforDAO;
import api.dao.pool.PoolSettingManager;
import api.entity.AdminEntity;
import api.entity.BaseRequestInfo;
import api.entity.BaseResponse;
import api.entity.CustomerInforEntity;
import api.entity.LoginResponse;
import api.enums.APIStatusEnum;
import api.factory.BaseHandler;
import com.google.gson.JsonObject;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import static server.main.Main.initBusiness;
import static utils.ApiUtils.geoCodingGoogle;
import utils.FirebaseUtils;
import utils.GsonUtils;
import utils.LogUtils;

public class MarkInforStatusForCar extends BaseHandler {

    private static final Logger logger = Logger.getLogger("MarkInforStatus");
    private static final MarkInforStatusForCar instance = new MarkInforStatusForCar();

    private static BaseResponse response = new BaseResponse();

    public MarkInforStatusForCar() {

    }

    public static MarkInforStatusForCar getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new BaseResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                int id = Integer.parseInt(requestInfo.getParam("id"));
                int status = Integer.parseInt(requestInfo.getParam("status"));
                
                
                        
                String carsig = requestInfo.getParam("carsig");
                String username = requestInfo.getParam("drivername");

                if (!new CarDAO().checkCarSig(username, carsig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_CAR_SIG);
                } else {

                    markStatus(id, status, username);

                    //push notification
                    setResponseStatus(response, APIStatusEnum.SUCCESS);
                }
            }

        } catch (Exception ex) {
            logger.error("MarkInforStatus-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

    private void markStatus(int id, int status, String username) throws Exception {

        CustomerInforDAO dao = new CustomerInforDAO();
        dao.markStatus(id, status, username);
        
        if (status == 0) {
            dao.updateActor(id, "");
        }
        
        CustomerInforEntity entitty = dao.getInforByID(id);
        FirebaseUtils.updateCustomerInfo(entitty);
    }
    
    public static void main(String args[]) throws Exception {
        
        LogUtils.init();

        initBusiness();
        
        if (!PoolSettingManager.loadBargDB()) {
            throw new Exception("load setting db config fail");
        }
        CustomerInforEntity entity = new CustomerInforDAO().getInforByID(1);
        
        System.out.println(GsonUtils.toJsonString(entity));
    }

}
