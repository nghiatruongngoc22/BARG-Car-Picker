/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.handler;

import api.config.InitFileConfig;
import api.dao.CarDAO;
import api.dao.CustomerInforDAO;
import api.entity.BaseRequestInfo;
import api.entity.CarEntity;
import api.entity.CustomerInforEntity;
import api.entity.GetCarLocationResponse;
import api.enums.APIStatusEnum;
import api.enums.CustomerInforStatus;
import api.factory.BaseHandler;
import org.apache.log4j.Logger;
import utils.FirebaseUtils;
import utils.GsonUtils;

public class BeginTravelingHandler extends BaseHandler {

    private static final Logger logger = Logger.getLogger("UpdateCarLocation");
    private static final BeginTravelingHandler instance = new BeginTravelingHandler();

    private static GetCarLocationResponse response = new GetCarLocationResponse();

    public BeginTravelingHandler() {

    }

    public static BeginTravelingHandler getInstance() {
        return instance;
    }

    @Override
    public String handle(BaseRequestInfo requestInfo) {
        try {

            response = new GetCarLocationResponse();

            if (!validateParam(requestInfo)) {
                setResponseStatus(response, APIStatusEnum.PARAM_INVALID);
            } else {
                int carID = Integer.parseInt(requestInfo.getParam("carid"));

                String carSig = requestInfo.getParam("carsig");

                String username = requestInfo.getParam("drivername");

                int id = Integer.parseInt(requestInfo.getParam("id"));

                if (!new CarDAO().checkCarSig(username, carSig)) {
                    setResponseStatus(response, APIStatusEnum.INVALID_CAR_SIG);
                } else {
                    try {
                        beginTraveling(carID, id, username);
                        setResponseStatus(response, APIStatusEnum.SUCCESS);
                    } catch (Exception ex) {
                        logger.error("BeginTravelingHandler-handle-ERROR: " + ex.getMessage(), ex);
                        setResponseStatus(response, APIStatusEnum.EXCEPTION);
                    }
                }

            }

        } catch (Exception ex) {
            logger.error("BeginTravelingHandler-handle-ERROR: " + ex.getMessage(), ex);
            setResponseStatus(response, APIStatusEnum.EXCEPTION);
        }

        return GsonUtils.toJsonString(response);

    }

    private void beginTraveling(int carID, int customerID, String driverName) throws Exception {
        CarDAO dao = new CarDAO();
        CustomerInforDAO customerDao = new CustomerInforDAO();

        CarEntity car = dao.getCarByID(carID);
        CustomerInforEntity entity = customerDao.getInforByID(customerID);
        
        if (entity.getRejectCount() + 1 >= InitFileConfig.rejectCountCfg) {
            setResponseStatus(response, APIStatusEnum.SERVER_HANDLER);
            return;
        }
        

        if (entity.getStatus() != CustomerInforStatus.VERIFIED.getValue()) {
            setResponseStatus(response, APIStatusEnum.ANOTHER_DRIVER_TOOK);
            return;
        }
        
        car.setIsBusy(1);
        car.setPickAddress(entity.getAddress());

        new CarDAO().updateCar(car.getId(), car.getIsBusy(), car.getPickAddress());
        entity.setCarID(car.getId());
        customerDao.markStatus(customerID, CustomerInforStatus.BEGIN_TRAVEL.getValue(), driverName);

        customerDao.updateInfo(customerID, car.getId());
        
        entity.setStatus(CustomerInforStatus.BEGIN_TRAVEL.getValue());
        entity.setAdminActor(driverName);

        FirebaseUtils.updateCustomerInfo(entity);
    }

    private boolean validateParam(BaseRequestInfo requestInfo) {
        return true;
    }

}
