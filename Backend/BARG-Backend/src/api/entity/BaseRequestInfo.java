/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.entity;

import java.util.HashMap;
import java.util.Map;

public class BaseRequestInfo<T> {

    private String methodName;
    private Map<String, T> params;
    private String bodyData;

    private final Map<String, String> exInfo = new HashMap<>();

    public BaseRequestInfo(Map<String, T> params) {
        this.params = params;
    }

    public BaseRequestInfo(String methodName, Map<String, T> params) {
        this.params = params;
        this.methodName = methodName;
    }

    public BaseRequestInfo(String bodyData) {
        this.bodyData = bodyData;
    }

    public String getBodyData() {
        return bodyData;
    }

    public String getParam(String param) {
        String result;
        T value = params.get(param);
        if (value == null) {
            return "";
        }
        if (value instanceof String) {
            result = (String) value;
        } else {
            result = ((String[]) value)[0];
        }

        if (result == null) {
            result = "";
        }
        return result;
    }

    public String getPostData() {
        return null;
    }

    public String getMethodName() {
        return methodName;
    }

    public Map<String, String> getParams() {
        final Map<String, String> p = new HashMap<>();
        for (Map.Entry<String, T> entry : params.entrySet()) {
            p.put(entry.getKey(), getParam(entry.getKey()));
        }
        return p;
    }

    public void putExInfo(String key, String value) {
        this.exInfo.put(key, value);
    }

    public String getExInfo(String key) {
        return this.exInfo.get(key);
    }

    public int getIntParam(String param) {
        return Integer.parseInt(getParam(param));
    }

    public long getLongParam(String param) {
        return Long.parseLong(getParam(param));
    }
}
