/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.entity;

import java.io.Serializable;

/**
 *
 * @author NghiaTruongNgoc
 */
public class AdminEntity implements Serializable {

    private String username = "";
    private String adminsig = "";
    private String description = "";
    private int role;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAdminsig() {
        return adminsig;
    }

    public void setAdminsig(String adminsig) {
        this.adminsig = adminsig;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

}
