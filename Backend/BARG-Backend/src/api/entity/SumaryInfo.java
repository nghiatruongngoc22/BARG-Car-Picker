/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.entity;

/**
 *
 * @author NghiaTruongNgoc
 */
public class SumaryInfo extends BaseMessage {

    private CarEntity car;
    private CustomerInforEntity customerinfo;

    public CarEntity getCar() {
        return car;
    }

    public void setCar(CarEntity car) {
        this.car = car;
    }

    public CustomerInforEntity getCustomerinfo() {
        return customerinfo;
    }

    public void setCustomerinfo(CustomerInforEntity customerinfo) {
        this.customerinfo = customerinfo;
    }

}
