/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.entity;

/**
 *
 * @author NghiaTruongNgoc
 */
public class GetVerifiedCustomerInforResponse extends GetCustomerInforResponse {

    private CarEntity car_info = new CarEntity();

    public CarEntity getCar_info() {
        return car_info;
    }

    public void setCar_info(CarEntity car_info) {
        this.car_info = car_info;
    }

}
