/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author NghiaTruongNgoc
 */
public class GetAllCustomerInfoResponse extends BaseResponse {

    private List<CustomerInforEntity> customerinfo_list = new ArrayList<>();

    public List<CustomerInforEntity> getCustomerinfo_list() {
        return customerinfo_list;
    }

    public void setCustomerinfo_list(List<CustomerInforEntity> customerinfo_list) {
        this.customerinfo_list = customerinfo_list;
    }

}
