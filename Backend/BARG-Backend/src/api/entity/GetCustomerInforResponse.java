/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.entity;

/**
 *
 * @author NghiaTruongNgoc
 */
public class GetCustomerInforResponse extends BaseResponse {

    private CustomerInforEntity customerinfo;

    public CustomerInforEntity getCustomerinfo() {
        return customerinfo;
    }

    public void setCustomerinfo(CustomerInforEntity customerinfo) {
        this.customerinfo = customerinfo;
    }

}
