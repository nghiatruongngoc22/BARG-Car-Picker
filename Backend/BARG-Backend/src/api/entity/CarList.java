/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.entity;

import java.util.List;

/**
 *
 * @author NghiaTruongNgoc
 */
public class CarList extends BaseMessage {

    private List<CarEntity> carlist;

    public List<CarEntity> getCarlist() {
        return carlist;
    }

    public void setCarlist(List<CarEntity> carlist) {
        this.carlist = carlist;
    }

}
