/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.constant;

/**
 *
 * @author NghiaTruongNgoc
 */
public class APINames {

    //api
    public static final String SAMPLE_API = "sample";
    public static final String LOGIN_API = "login";
    public static final String LOGOUT_API = "logout";
    public static final String AVAILABLE_CAR_LIST = "availablecarlist";
    public static final String MARK_CUSTOMER_INFO = "markcustomerinfo";
    public static final String ADD_CUSTOMER_INFO = "addcustomerinfo";
    public static final String GET_CUSTOMER_INFO = "getcustomerinfo";
    public static final String GEO_CODING = "geocoding";
    public static final String REVERSE_GEO_CODING = "reversegeocoding";
    public static final String GET_VERIFIED_INFO = "getverifiedinfo";
    public static final String GET_ALL_CUSTOMER_INFO = "getallcustomerinfo";
    public static final String GET_ALL_CUSTOMER_INFO_BY_PHONE = "getallcustomerinfobyphone";
    public static final String VERIFY_CUSTOMER_INFO = "verifycustomerinfo";

    public static final String BEGIN_TRAVELING = "begintraveling";
    public static final String END_TRAVELING = "endtraveling";
    public static final String REJECT_TRAVELING = "rejecttraveling";
    public static final String CAR_LOGIN = "carlogin";
    public static final String CAR_LOGOUT = "carlogout";
    public static final String UPDATE_CAR_LOCATION = "updatecarlocation";
    public static final String GET_CAR_LOCATION = "getcarlocation";

}
