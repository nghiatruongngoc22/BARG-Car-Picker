/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.factory;

import api.entity.BaseRequestInfo;
import api.entity.BaseResponse;
import api.enums.APIStatusEnum;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import server.http.BaseServletHandler;

public abstract class BaseHandler extends BaseServletHandler {

    private static final Logger logger = Logger.getLogger("BaseHandler");
    

    public enum DATA_REQUEST_TYPE {
        PARAM, BODY;
    }

    protected DATA_REQUEST_TYPE getDataRequestType() {
        return DATA_REQUEST_TYPE.PARAM;
    }
    
    protected void setResponseStatus(BaseResponse response, APIStatusEnum e) {
        response.setReturncode(e.getValue());
        response.setReturnmessage(e.getMessage());
    }


    @Override
    public String handle(HttpServletRequest req) {
        BaseRequestInfo<String> requestInfo = null;
        if (getDataRequestType() == DATA_REQUEST_TYPE.PARAM) {
            requestInfo = new BaseRequestInfo(req.getParameterMap());
        } else {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();

                String json = "";
                while (json != null) {
                    json = br.readLine();
                    if (json != null) {
                        stringBuilder.append(json);
                    }
                }
                requestInfo = new BaseRequestInfo(stringBuilder.toString());
            } catch (IOException ex) {
                logger.error("Read body data error", ex);
            }
        }
        return handle(requestInfo);
    }

    abstract public String handle(BaseRequestInfo req);
}
