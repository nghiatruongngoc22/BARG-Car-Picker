package api.factory;

public class HandlerFactoryManagement {

    public static IHandlerFactory getFactory(String pathName) {
        IHandlerFactory factory = null;
        if (pathName == null || pathName.isEmpty()) {
            return factory;
        }
        pathName = pathName.toLowerCase();

        switch (pathName) {
            case "barg":
                factory = BargHandlerFactory.getInstance();
                break;
        }

        return factory;
    }
}
