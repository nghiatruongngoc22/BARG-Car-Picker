package api.factory;

public interface IHandlerFactory {

    public BaseHandler getHandler(String methodName);
}
