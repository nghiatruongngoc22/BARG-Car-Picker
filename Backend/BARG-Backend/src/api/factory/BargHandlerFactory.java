package api.factory;

import api.config.InitFileConfig;
import api.constant.APINames;
import api.handler.*;

public class BargHandlerFactory implements IHandlerFactory {

    public static BargHandlerFactory instance = new BargHandlerFactory();

    public static IHandlerFactory getInstance() {
        return instance;
    }

    @Override
    public BaseHandler getHandler(String methodName) {
        BaseHandler handler = null;
        if (methodName == null || methodName.isEmpty()) {
            return null;
        }

        methodName = methodName.toLowerCase();

        switch (methodName) {
            case APINames.SAMPLE_API:
                handler = SampleHandler.getInstance();
                break;
            case APINames.LOGIN_API:
                handler = LoginHandler.getInstance();
                break;
            case APINames.LOGOUT_API:
                handler = LogoutHandler.getInstance();
                break;

            case APINames.AVAILABLE_CAR_LIST:
                handler = GetAvailableCarListHandler.getInstance();
                break;

            case APINames.MARK_CUSTOMER_INFO:
                handler = MarkInforStatus.getInstance();
                break;

            case APINames.ADD_CUSTOMER_INFO:
                handler = AddCustomerInfor.getInstance();
                break;

            case APINames.GET_CUSTOMER_INFO:
                handler = GetCustomerInfor.getInstance();
                break;

            case APINames.GEO_CODING:
                handler = GeoCodingHandler.getInstance();
                break;

            case APINames.REVERSE_GEO_CODING:
                handler = ReverseGeoCodingHandler.getInstance();
                break;

            case APINames.GET_VERIFIED_INFO:
                handler = GetVerifiedCustomerInfor.getInstance();
                break;

            case APINames.GET_ALL_CUSTOMER_INFO:
                handler = GetAllCustomerInfor.getInstance();
                break;
            case APINames.GET_ALL_CUSTOMER_INFO_BY_PHONE:
                handler = GetAllCustomerInforByPhone.getInstance();
                break;

            case APINames.VERIFY_CUSTOMER_INFO:
                handler = VerifyCustomerInfo.getInstance();
                break;

            case APINames.BEGIN_TRAVELING:
                handler = BeginTravelingHandler.getInstance();
                break;

            case APINames.END_TRAVELING:
                handler = EndTravelingHandler.getInstance();
                break;

            case APINames.REJECT_TRAVELING:
                handler = RejectTravelingHandler.getInstance();
                break;

            case APINames.CAR_LOGIN:
                handler = CarLoginHandler.getInstance();
                break;

            case APINames.CAR_LOGOUT:
                handler = CarLogoutHandler.getInstance();
                break;

            case APINames.UPDATE_CAR_LOCATION:
                handler = UpdateCarDriverLocation.getInstance();
                break;

            case APINames.GET_CAR_LOCATION:
                handler = GetCarLocation.getInstance();
                break;

        }
        return handler;
    }
}
