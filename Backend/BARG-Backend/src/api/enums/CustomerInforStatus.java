package api.enums;

import java.util.HashMap;

public enum CustomerInforStatus {
    
    NOT_HANDLE_YET(0, "Not be handled yet."),
    PROCESSING(1, "Processing"),
    VERIFIED(2, "Finished locating"),
    
    BEGIN_TRAVEL(3, "Begin traveling"),
    END_TRAVEL(4, "End Traveling"),
    CAR_NOT_FOUND(5, "Car not found");

    private final int value;
    private final String message;
    private static final HashMap<Integer, CustomerInforStatus> returnMap = new HashMap();

    static {
        for (CustomerInforStatus returnCodeEnum : CustomerInforStatus.values()) {
            returnMap.put(returnCodeEnum.value, returnCodeEnum);
        }
    }

    private CustomerInforStatus(int value, String message) {
        this.value = value;
        this.message = message;
    }

    public int getValue() {
        return value;
    }

    public static CustomerInforStatus fromInt(int value) {
        return returnMap.get(value);
    }

    public String getMessage() {
        return message;
    }

}
