package api.enums;

import java.util.HashMap;

public enum APIStatusEnum {
    
    EMPTY_CAR_SERVER_HANDLING(3, "Theres no car left to choose by servers"),
    SERVER_HANDLING(2, "Server is handling case."),
    SUCCESS(1, "Successful."),
    PARAM_INVALID(-1, "Invalid Param."),
    ADMIN_NOT_FOUND(-2, "Invalid username or password."),
    INVALID_ADMIN_SIG(-3, "Invalid AdminSig, Access Denined."),
    HANDLED_INFOR(-4, "Infor is being processed or finished"),
    NOT_HANDLED_INFOR(-5, "Infor is not VERIFIED"),
    
    CAR_NOT_FOUND(-9, "Car not found"),
    DRIVER_NOT_FOUND(-10, "Invalid username or password OF DRIVER"),
    INVALID_CAR_SIG(-11, "Driver Sig invalid"),
    ANOTHER_DRIVER_TOOK(-12, "Another Drive took this"),
    NO_DRIVER(-13, "No driver wants this"),
    SERVER_HANDLER(-14, "Server handle it."),
    EXCEPTION(0, "System Error.");

    private final int value;
    private final String message;
    private static final HashMap<Integer, APIStatusEnum> returnMap = new HashMap();

    static {
        for (APIStatusEnum returnCodeEnum : APIStatusEnum.values()) {
            returnMap.put(returnCodeEnum.value, returnCodeEnum);
        }
    }

    private APIStatusEnum(int value, String message) {
        this.value = value;
        this.message = message;
    }

    public int getValue() {
        return value;
    }

    public static APIStatusEnum fromInt(int value) {
        return returnMap.get(value);
    }

    public String getMessage() {
        return message;
    }
    

}
